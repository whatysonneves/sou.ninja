<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class LandingController extends Controller
{
	public function ton(Request $req)
	{
		$data = [
			"date" => 1633482000,
			"pos" => [
				[
					"name" => "Cielo ZIP",
					"debit" => 2.39,
					"credit1x" => 4.99,
					"credit2x" => 11.57,
					"credit3x" => 14.56,
					"credit4x" => 17.55,
					"credit5x" => 20.54,
					"credit6x" => 23.53,
					"credit7x" => 26.52,
					"credit8x" => 29.51,
					"credit9x" => 32.5,
					"credit10x" => 35.49,
					"credit11x" => 38.48,
					"credit12x" => 41.47,
				],
				[
					"name" => "Mercado Pago Point",
					"debit" => 1.99,
					"credit1x" => 4.74,
					"credit2x" => 9.91,
					"credit3x" => 11.29,
					"credit4x" => 12.07,
					"credit5x" => 13.76,
					"credit6x" => 15.45,
					"credit7x" => 15.74,
					"credit8x" => 17.23,
					"credit9x" => 18.72,
					"credit10x" => 20.21,
					"credit11x" => 21.7,
					"credit12x" => 23.19,
				],
				[
					"name" => "PagSeguro Minizinha",
					"debit" => 1.99,
					"credit1x" => 4.99,
					"credit2x" => 9.91,
					"credit3x" => 11.29,
					"credit4x" => 12.64,
					"credit5x" => 13.97,
					"credit6x" => 15.27,
					"credit7x" => 16.55,
					"credit8x" => 17.81,
					"credit9x" => 19.04,
					"credit10x" => 20.24,
					"credit11x" => 21.43,
					"credit12x" => 22.59,
				],
				[
					"name" => "SafraPay",
					"debit" => 1.88,
					"credit1x" => 4.49,
					"credit2x" => 7.45,
					"credit3x" => 9.8,
					"credit4x" => 9.8,
					"credit5x" => 10.6,
					"credit6x" => 12.1,
					"credit7x" => 13.6,
					"credit8x" => 15.1,
					"credit9x" => 16.6,
					"credit10x" => 16.88,
					"credit11x" => 16.88,
					"credit12x" => 16.88,
				],
				[
					"name" => "SumUp",
					"debit" => 1.9,
					"credit1x" => 4.6,
					"credit2x" => 6.1,
					"credit3x" => 7.6,
					"credit4x" => 9.1,
					"credit5x" => 10.6,
					"credit6x" => 12.1,
					"credit7x" => 13.6,
					"credit8x" => 15.1,
					"credit9x" => 16.6,
					"credit10x" => 18.1,
					"credit11x" => 19.6,
					"credit12x" => 21.1,
				],
				[
					"name" => "Ton Giga",
					"debit" => 1.45,
					"credit1x" => 2.9,
					"credit2x" => 3.59,
					"credit3x" => 3.93,
					"credit4x" => 4.26,
					"credit5x" => 4.59,
					"credit6x" => 4.92,
					"credit7x" => 5.83,
					"credit8x" => 6.16,
					"credit9x" => 6.48,
					"credit10x" => 6.81,
					"credit11x" => 7.13,
					"credit12x" => 7.44,
				],
			],
		];

		return view("landing/ton", compact("data"));
	}

	public function fno(Request $req)
	{
		$carbon = Carbon::now()->locale("pt_BR");
		$month = $carbon->monthName;
		$year = $carbon->format("Y");
		$days = [$carbon->format("d"), $carbon->subDay()->format("d"), $carbon->subDay()->format("d")];
		$countdown = $this->getCountdownTime($req);
		return view("landing/fno", compact("month", "year", "days", "countdown"));
	}

	protected function setTimestamp($req)
	{
		$time = time();
		$response = new Response();
		$response->headers->setCookie(new Cookie("fno_access_time", $time));
		$response->send();
		return $time;
	}

	protected function getTimestamp($req)
	{
		if($req->hasCookie("fno_access_time")) {
			return $req->cookie("fno_access_time");
		}
		return $this->setTimestamp($req);
	}

	protected function getCountdownTime($req)
	{
		$time = $this->getTimestamp($req);
		$carbon = Carbon::createFromTimestamp($time)->addHours(7)->addMinutes(31)->addSeconds(50);
		if(Carbon::now()->diffInHours($carbon) < 1) {
			$carbon = Carbon::createFromTimestamp($this->setTimestamp($req))->addHours(7)->addMinutes(31)->addSeconds(50);
		}
		return $carbon->format("Y/m/d H:i:s");
	}
}
