<!DOCTYPE html>
<html class="" lang="pt-BR">
<head>
	<base href="https://www.ton.com.br/" />
	<meta name="theme-color" content="#00C700">
	<meta name="url" content="https://www.ton.com.br/">
	<link rel="shortcut icon" href="https://www.ton.com.br/img/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="https://www.ton.com.br/img/ton-stone.png">
	<link rel="dns-prefetch" href="***//res.cloudinary.com">
	<link rel="preconnect" href="***//res.cloudinary.com">
	<script type="application/ld+json">{"@context":"https://schema.org","@type":"FAQPage","mainEntity":[{"@type":"Question","name":"Como funciona a taxa única do Ton?","acceptedAnswer":{"@type":"Answer","text":"Você só paga a taxa única se optar por adquirir uma maquininha. O aplicativo Ton é gratuito.\nA taxa única pode ser dividida em até 12x no cartão de crédito ou à vista no boleto bancário.Após este pagamento, não haverá mais cobrança fixa nenhuma pelo uso da máquina, nem mesmo aluguel! A maquininha continua sendo do Ton e, por isso, assumimos total responsabilidade sobre sua manutenção, oferecendo suporte ilimitado em casos de quebra ou dano do equipamento."}},{"@type":"Question","name":"Qual é o prazo de entrega da minha maquininha?","acceptedAnswer":{"@type":"Answer","text":"O prazo de entrega da sua maquininha para as regiões Sudeste, Sul e Centro-Oeste é de até 10 dias úteis e para as regiões Nordeste e Norte é de até 12 dias úteis."}},{"@type":"Question","name":"Eu posso escolher a operadora do meu chip?","acceptedAnswer":{"@type":"Answer","text":"Sim! Na hora de adquirir a maquininha com Chip 2G/3G e Wi-Fi, você pode escolher entre as operadoras Claro, Oi, Vivo e Tim."}},{"@type":"Question","name":"Eu preciso de um celular para realizar as vendas?","acceptedAnswer":{"@type":"Answer","text":"Se você adquiriu a maquininha com conexão Bluetooth ou o link de pagamentos, precisará de um celular ou tablet com sistema Android (versão 4.1 ou superior) ou iOs para realizar suas vendas.\nCaso você tenha escolhido o modelo com Chip 2G /3G e Wi-Fi ou a maquininha de bobina, não será necessário usar um celular para realizar as vendasImportante: para ativar a maquininha de bobina, você precisará baixar o aplicativo!\nPara todos os modelos, você precisará de um aparelho para baixar o aplicativo Ton e realizar as demais funcionalidades, como transferências,escolha do modelo de recebimento, entre outras."}},{"@type":"Question","name":"O aplicativo Ton é compatível com quais aparelhos?","acceptedAnswer":{"@type":"Answer","text":"O aplicativo funciona em qualquer celular ou tablet com sistema Android 4.1 ou superior e iOs."}},{"@type":"Question","name":"Como receberei o valor das minhas vendas?","acceptedAnswer":{"@type":"Answer","text":"Depois que suas vendas estiverem disponíveis na sua conta Ton, você poderá realizar transferências do saldo para sua conta bancária ou cartão pré-pago.\nO valor máximo para transferência para o seu cartão de pré-pago é de R$ 5.000,00\nImportante: para transferir o saldo disponível é necessário que a conta seja de mesma titularidade do CNPJ ou CPF cadastrado no aplicativo Ton."}},{"@type":"Question","name":"Quais tipos de contas bancárias são aceitos para transferência?","acceptedAnswer":{"@type":"Answer","text":"Conta Corrente (exceto conta salário);\nConta Poupança;\nConta Corrente Conjunta;\nConta Poupança Conjunta.\nImportante: a conta deve ser de titularidade do CNPJ ou CPF do cadastrado no Ton."}},{"@type":"Question","name":"Qual é o prazo de garantia da minha maquininha?","acceptedAnswer":{"@type":"Answer","text":"Não há prazo de garantia! Uma vez que a taxa única de adesão for paga, será possível realizar trocas por quaisquer problemas técnicos e/ou de fabricação."}},{"@type":"Question","name":"Quais problemas são cobertos pela garantia?","acceptedAnswer":{"@type":"Answer","text":"A garantia cobre quaisquer problemas técnicos ou danos de fabricação. Defeitos causados por mau uso ou uso indevido do leitor não serão cobertos pela garantia."}},{"@type":"Question","name":"Qual é o prazo para troca da minha maquininha?","acceptedAnswer":{"@type":"Answer","text":"Em caso de problemas técnicos ou defeitos de fabricação, o prazo de troca da maquininha será de até 10 dias úteis."}},{"@type":"Question","name":"Minha maquininha está com defeito. Como realizo a troca?","acceptedAnswer":{"@type":"Answer","text":"Caso sua maquininha apresente quaisquer problemas técnicos ou defeitos de fabricação, entre em contato com o nosso Time de Relacionamento pelo chat do seu aplicativo ou aqui no site.\nSe preferir, você pode enviar um email para ton@stone.com.br\nO atendimento humano funciona de segunda à sexta das 6h às 23h e aos sábados e domingos das 8h às 20h. O Ton, nosso assistente, está pronto para te atender 24 horas por dia.\nImportante: não será realizada troca de maquininhas com defeitos causados por mau uso ou uso indevido."}},{"@type":"Question","name":"Como realizo o primeiro acesso no aplicativo?","acceptedAnswer":{"@type":"Answer","text":"Para realizar o primeiro acesso do seu aplicativo, siga os passos abaixo:\n- Clique em 'Entrar';\n- Digite o seu e-mail e clique em 'Continuar';\n- Clique em 'Meu primeiro acesso aqui';\n- Digite o código de primeiro acesso que te enviarmos via SMS e e-mail;\n- Crie a sua senha! Ela precisa ter 8 caracteres (com pelo menos uma letra e um número). Confirme a sua senha digitando-a novamente;\n- Clique em “Criar senha”.\nPronto, senha definida!"}},{"@type":"Question","name":"Como conecto minha Maquininha T1 com conexão via Bluetooth ao meu celular ou tablet?","acceptedAnswer":{"@type":"Answer","text":"Somente a Máquina com conexão via Bluetooth precisa ser conectada ao celular ou tablet.\nCaso você possua a Máquina com conexão via Bluetooth, realize o login no aplicativo para então seguir os passos abaixo:\n- Ligue o Bluetooth do seu celular ou tablet;\n- Ligue sua maquininha ao pressionar e segurar o botão superior do lado direto e em seguida aperte o botão “0” para visualizar o ID da maquininha;\n- Verifique se uma imagem de cadeado está presente no visor da sua maquininha. Se estiver aparecendo, significa que a maquininha está bloqueada. Para desbloquear, aperte o botão de ligar e desligar que está localizado na parte superior do aparelho;\n- Na tela inicial do aplicativo, clique no ícone de “Configurações” (localizado na parte superior direita da tela), e em seguida selecione “Configurar maquininha”. Escolha a opção: PAX D150.\nA partir daí, é só seguir o tutorial para que aparecerá na tela de seu celular!\nATENÇÃO: Caso as informações da maquininha apaguem antes de você concluir o processo, basta pressionar o botão “0”, e o ID e a senha reaparecerão."}},{"@type":"Question","name":"Como configuro minha Máquina T2/T3 com chip 2G/3G?","acceptedAnswer":{"@type":"Answer","text":"Na tela inicial do aplicativo Ton:\n- Clique no ícone de “Configurações” (localizado na parte superior direita da tela), e em “Configurar maquininha”;\n- Em seguida, selecione o modelo correspondente a sua máquina;\n- O ID Ton aparecerá na tela de seu celular.\nNa sua máquina:\n- Aperte o botão verde, escolha a opção “Menu ADM” (Opção 8), e depois vá em “Ativação” (Opção 1). Neste momento, será solicitado que você digite o ID Ton, que é o ID que estará aparecendo no seu aplicativo. Assim que você digitar o seu ID, seus dados aparecerão na tela de sua máquina, então você deverá apertar o botão verde novamente.\nPronto! Sua maquininha está ativada!"}},{"@type":"Question","name":"Como ativo a minha maquininha de bobina?","acceptedAnswer":{"@type":"Answer","text":"- Com a maquininha ligada, clique na opção “Vamos começar” que aparece na tela;\n- Selecione sua opção de conexão: Chip 3G ou Wi-Fi;\n- No aplicativo, vá até “Configurações” e depois em “Ativar maquininha”;\n- Na maquininha, digite o código de ativação que aparece no aplicativo;\n- Cadastre sua senha administrativa.Aguarde alguns segundos para a maquininha ativar e depois estará pronta para vender!"}},{"@type":"Question","name":"Como ativo o meu cartão pré-pago?","acceptedAnswer":{"@type":"Answer","text":"- Na tela inicial do aplicativo, selecione a opção “Cartões”;\n- Clique em “Adicionar cartão”;\n- Digite o número do cartão, a validade e o CVV, e depois clique em 'Continuar'.\nPronto! Se o seu cartão aparecer na tela do aplicativo, significa que ele foi ativado!"}}]}</script>
	<style>.async-hide{opacity:0!important}</style>
	<meta name="viewport" content="width=device-width">
	<meta charset="utf-8">
	<meta property="og:url" content="https://ton.com.br/">
	<meta property="og:type" content="website">
	<meta property="og:image" content="https://res.cloudinary.com/dunz5zfpt/image/upload/v1/site-ton/OGImage.png">
	<meta property="og:image:width" content="800">
	<meta property="og:image:height" content="600">
	<meta property="og:locale" content="pt_BR">
	<meta property="og:site_name" content="Ton">
	<title>Maquininha de Cartão de Crédito - Peça sua Maquineta | Ton</title>
	<meta name="robots" content="index,follow">
	<meta name="googlebot" content="index,follow">
	<meta name="description" content="A melhor Maquininha de Cartão com o menor preço! Peça sua maquineta na Ton e aceite cartões de crédito e débito com as menores taxas. Confira!">
	<meta property="og:title" content="Maquininha de Cartão de Crédito - Peça sua Maquineta | Ton">
	<meta property="og:description" content="A melhor Maquininha de Cartão com o menor preço! Peça sua maquineta na Ton e aceite cartões de crédito e débito com as menores taxas. Confira!">
	<link rel="canonical" href="https://ton.com.br/">
	<script type="application/ld+json">{"@context":"https://schema.org/","@type":"FAQPage","mainEntity":[{"@type":"Question","name":"Como funciona a taxa única de adesão do Ton?","acceptedAnswer":{"@type":"Answer","text":"Não temos mensalidade! Você paga a taxa apenas ao aderir uma das máquinas em modelo de comodato, tendo direito a garantia enquanto durar nossa parceria.\nA adesão da máquina pode ser paga à vista ou parcelada em até 12 vezes no cartão de crédito. Após este pagamento, não haverá cobrança fixa (aluguel) pelo uso da máquina.\nO cadastro no aplicativo é gratuito e dá direito a utilização de funções como o link de pagamento. Você só paga a taxa única se optar por adquirir uma maquininha."}},{"@type":"Question","name":"Qual é o prazo de entrega da minha maquininha?","acceptedAnswer":{"@type":"Answer","text":"O prazo de entrega das nossas maquininhas pode variar de acordo com o modelo escolhido, sendo de até 7 dias úteis para a máquina T1 e até 20 dias úteis para as máquinas T2 e T3. O cartão pré-pago, se solicitado no aplicativo, é enviado separadamente e entregue em até 20 dias úteis."}},{"@type":"Question","name":"Eu preciso de um celular para realizar as vendas?","acceptedAnswer":{"@type":"Answer","text":"Apenas a máquina T1 precisa de um celular Android ou iOS, com bluetooth e conexão de internet para poder realizar suas vendas, e as máquinas T2 e T3, não precisam de celular para realizar as vendas.\nVocê precisará de um celular Android ou iOS com o aplicativo do Ton instalado para ativar qualquer uma das máquinas, gerenciar suas vendas, realizar transferências, entre outras funcionalidades."}},{"@type":"Question","name":"Como receberei o valor das minhas vendas?","acceptedAnswer":{"@type":"Answer","text":"O valor das vendas estarão disponíveis em sua carteira no prazo escolhido para o recebimento, podendo ser de 1 dia útil, 14 dias ou 30 dias, de acordo com a sua preferência.\nDepois disso você poderá transferir para sua conta bancária cadastrada, sem custos, ou para o seu cartão pré-pago (disponível apenas para pessoa física).\nImportante: para transferir o saldo disponível é necessário que a conta bancária seja de mesma titularidade do CNPJ ou CPF cadastrado no aplicativo Ton.\nTipos de contas bancárias aceitas para transferência:\nConta Corrente (exceto conta salário);\nConta Poupança;\nConta Corrente Conjunta;\nConta Poupança Conjunta."}},{"@type":"Question","name":"Como funciona a garantia da minha maquininha?","acceptedAnswer":{"@type":"Answer","text":"A garantia é ilimitada e cobre qualquer problema técnico ou dano de fabricação. Defeitos causados por mau uso ou uso indevido do leitor não serão cobertos pela garantia.\nEm caso de problemas técnicos ou defeitos de fabricação, entre em contato com o nosso atendimento pelo chat do seu aplicativo."}},{"@type":"Question","name":"Eu posso escolher a operadora do meu chip?","acceptedAnswer":{"@type":"Answer","text":"Sim. Na hora de pedir sua máquina T2 ou T3, você pode escolher entre as operadoras Claro, Vivo ou Tim."}},{"@type":"Question","name":"O aplicativo Ton é compatível com quais dispositivos?","acceptedAnswer":{"@type":"Answer","text":"O aplicativo funciona em qualquer dispostivo Android versão 5.0 ou superior, ou iOS versão 11 ou superior."}}]}</script>
	<meta name="next-head-count" content="17">
	<link rel="preload" href="https://www.ton.com.br/_next/static/css/446e10d0487e6e92b9be.css" as="style">
	<link rel="stylesheet" href="https://www.ton.com.br/_next/static/css/446e10d0487e6e92b9be.css" data-n-g="">
	<link rel="preload" href="https://www.ton.com.br/_next/static/css/bc130ff4a128f6b42612.css" as="style">
	<link rel="stylesheet" href="https://www.ton.com.br/_next/static/css/bc130ff4a128f6b42612.css" data-n-p="">
	<link rel="preload" href="https://www.ton.com.br/_next/static/css/47405c16a540aa1e907a.css" as="style">
	<link rel="stylesheet" href="https://www.ton.com.br/_next/static/css/47405c16a540aa1e907a.css">
	<link rel="preload" href="https://www.ton.com.br/_next/static/css/bf1519bb25cef636d5f5.css" as="style">
	<link rel="stylesheet" href="https://www.ton.com.br/_next/static/css/bf1519bb25cef636d5f5.css">
	<link rel="preload" href="https://www.ton.com.br/_next/static/css/c8ba0b51d748231f869e.css" as="style">
	<link rel="stylesheet" href="https://www.ton.com.br/_next/static/css/c8ba0b51d748231f869e.css">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/9492.2992b511035a0967fd93.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/2550.c00df0c44b8785602d5a.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/7174.b64609cb624a67ee80da.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/8687.9e34e8ff6a971e59d6b9.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/8105.ac010b3120a3e6281cbb.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/957.ee54d989f86e59caf67e.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/9557.550f1459268d5e754608.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/7361.95b73a7123ad62b96b81.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/4281.d2056bb04c5e0d8c868a.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/4210.9800ea038e334500d790.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/1850.d66c3c7a68683433c5d5.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/8731.ed6cf65c38e67fedef91.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/221.d4660a6c3eebfb53e337.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/1824.aeac7f23bf47c82d58e0.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/6176-c42d952c50f2f6b42c33.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/webpack-f494fdd072920e76f5c3.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/framework-9260c7ce6eb8840e551e.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/main-d550d58322f8445a98e1.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/pages/_app-ef0d0b040a803001b83a.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/2455-28fdbde43e9e582db6d8.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/7901-baa24bff990ebbdf91ce.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/1268-6edd7988ebdc139c47ba.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/9591-2b894b26ef83de15f567.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/4687-4401d415952335433cad.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/8988-16d195709aa539216d84.js" as="script">
	<link rel="preload" href="https://www.ton.com.br/_next/static/chunks/pages/index-67d50b3c3c4795bfa92d.js" as="script">
	<style type="text/css">@font-face{font - family:'Gotham Rounded';src:url("https : //res.cloudinary.com/dunz5zfpt/raw/upload/v1616158158/fonts/GothamRounded/GothamRounded-Bold.woff2") format('woff2');font-weight:700;font-style:normal;font-display:swap}@font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmSU5fCRc4EsA.woff2") format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmSU5fABc4EsA.woff2") format('woff2');unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmSU5fCBc4EsA.woff2") format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmSU5fBxc4EsA.woff2") format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmSU5fCxc4EsA.woff2") format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmSU5fChc4EsA.woff2") format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:300;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmSU5fBBc4.woff2") format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu72xKOzY.woff2") format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu5mxKOzY.woff2") format('woff2');unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu7mxKOzY.woff2") format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu4WxKOzY.woff2") format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu7WxKOzY.woff2") format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu7GxKOzY.woff2") format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:400;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOmCnqEu92Fr1Mu4mxK.woff2") format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmEU9fCRc4EsA.woff2") format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmEU9fABc4EsA.woff2") format('woff2');unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmEU9fCBc4EsA.woff2") format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmEU9fBxc4EsA.woff2") format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmEU9fCxc4EsA.woff2") format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmEU9fChc4EsA.woff2") format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:500;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmEU9fBBc4.woff2") format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}@font-face{font-family:'Roboto';font-style:normal;font-weight:700;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfCRc4EsA.woff2") format('woff2');unicode-range:U+0460-052F,U+1C80-1C88,U+20B4,U+2DE0-2DFF,U+A640-A69F,U+FE2E-FE2F}@font-face{font-family:'Roboto';font-style:normal;font-weight:700;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfABc4EsA.woff2") format('woff2');unicode-range:U+0400-045F,U+0490-0491,U+04B0-04B1,U+2116}@font-face{font-family:'Roboto';font-style:normal;font-weight:700;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfCBc4EsA.woff2") format('woff2');unicode-range:U+1F00-1FFF}@font-face{font-family:'Roboto';font-style:normal;font-weight:700;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfBxc4EsA.woff2") format('woff2');unicode-range:U+0370-03FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:700;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfCxc4EsA.woff2") format('woff2');unicode-range:U+0102-0103,U+0110-0111,U+0128-0129,U+0168-0169,U+01A0-01A1,U+01AF-01B0,U+1EA0-1EF9,U+20AB}@font-face{font-family:'Roboto';font-style:normal;font-weight:700;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfChc4EsA.woff2") format('woff2');unicode-range:U+0100-024F,U+0259,U+1E00-1EFF,U+2020,U+20A0-20AB,U+20AD-20CF,U+2113,U+2C60-2C7F,U+A720-A7FF}@font-face{font-family:'Roboto';font-style:normal;font-weight:700;font-display:swap;src:url("https://fonts.gstatic.com/s/roboto/v27/KFOlCnqEu92Fr1MmWUlfBBc4.woff2") format('woff2');unicode-range:U+0000-00FF,U+0131,U+0152-0153,U+02BB-02BC,U+02C6,U+02DA,U+02DC,U+2000-206F,U+2074,U+20AC,U+2122,U+2191,U+2193,U+2212,U+2215,U+FEFF,U+FFFD}</style>
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/2455-28fdbde43e9e582db6d8.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/7901-baa24bff990ebbdf91ce.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/1268-6edd7988ebdc139c47ba.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/9591-2b894b26ef83de15f567.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/4687-4401d415952335433cad.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/8988-16d195709aa539216d84.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/pages/index-67d50b3c3c4795bfa92d.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/8172-c78084adaaab5e794e5a.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/pages/tapton-b1719b5a246b773b55d7.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/6176-c42d952c50f2f6b42c33.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/pages/planos-e-taxas-feb1972fc572cac0cd45.js">
	<link as="script" rel="prefetch" href="https://www.ton.com.br/_next/static/chunks/pages/maquininha/%5BmachineName%5D-f508bab831b3c2d24f7d.js">
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-header{background-color:var(--headerBackgroundColor);color:#fff;display:flex;flex-direction:column;justify-content:center;text-align:left;height:55px;font-family:Roboto,serif;position:relative;padding-left:15%}.push-conversation-container .push-header .push-avatar{height:31px;width:31px;position:absolute;top:12px;left:14px}.push-conversation-container .push-header .push-header-image{height:70%;width:50%;position:absolute;left:2px}.push-conversation-container .push-header.push-without-header-avatar .push-title-and-subtitle{margin-left:-10%}.push-conversation-container .push-header.push-with-subtitle{height:70px}.push-conversation-container .push-header.push-with-subtitle .push-avatar{top:20px}.push-conversation-container .push-header.push-with-subtitle span{top:46px;font-weight:300;font-size:13px;position:absolute}.push-conversation-container .push-header.push-with-subtitle span.push-with-avatar{top:65%}.push-conversation-container .push-title{font-size:20px;font-family:Roboto,serif;text-align:left;color:var(--titleColor)}.push-conversation-container .push-subtitle{color:var(--subtitleColor)}.push-conversation-container .push-header-buttons{position:absolute;right:10px;display:flex;flex-direction:row}.push-conversation-container .push-close-button{display:flex;justify-content:center;align-items:center;display:inline-block;background-color:var(--headerBackgroundColor);border:0;width:40px;cursor:pointer}.push-conversation-container .push-close-button:active,.push-conversation-container .push-close-button:focus,.push-conversation-container .push-close-button:hover{outline:none}.push-conversation-container .push-close-button:active::-moz-focus-inner,.push-conversation-container .push-close-button:focus::-moz-focus-inner,.push-conversation-container .push-close-button:hover::-moz-focus-inner{border:0}.push-conversation-container .push-close{width:30px;height:30px;padding-top:3px}.push-conversation-container .push-close.push-default{width:20px;height:20px}.push-conversation-container .push-close.push-default:active,.push-conversation-container .push-close.push-default:focus,.push-conversation-container .push-close.push-default:hover{width:24px;height:24px}.push-conversation-container .push-close:active,.push-conversation-container .push-close:focus,.push-conversation-container .push-close:hover{width:34px;height:34px;top:-2px;margin-left:-2px}.push-conversation-container .push-toggle-fullscreen-button{display:flex;justify-content:center;align-items:center;display:inline-block;background-color:var(--headerBackgroundColor);border:0;width:40px;cursor:pointer}.push-conversation-container .push-toggle-fullscreen-button:active,.push-conversation-container .push-toggle-fullscreen-button:focus,.push-conversation-container .push-toggle-fullscreen-button:hover{outline:none}.push-conversation-container .push-toggle-fullscreen-button:active::-moz-focus-inner,.push-conversation-container .push-toggle-fullscreen-button:focus::-moz-focus-inner,.push-conversation-container .push-toggle-fullscreen-button:hover::-moz-focus-inner{border:0}.push-conversation-container .push-toggle-fullscreen{width:30px;height:30px;padding-top:3px}.push-conversation-container .push-toggle-fullscreen.push-default{width:20px;height:20px}.push-conversation-container .push-toggle-fullscreen.push-default:active,.push-conversation-container .push-toggle-fullscreen.push-default:focus,.push-conversation-container .push-toggle-fullscreen.push-default:hover{width:24px;height:24px}.push-conversation-container .push-toggle-fullscreen:active,.push-conversation-container .push-toggle-fullscreen:focus,.push-conversation-container .push-toggle-fullscreen:hover{width:34px;height:34px;top:-2px;margin-left:-2px}.push-conversation-container .push-loading{background-color:#b5b5b5;color:#fff;display:flex;flex-direction:column;text-align:center;padding:5px 0;font-family:Roboto,serif;font-size:14px}.push-widget-embedded .push-header{display:none}.push-full-screen .push-header{border-radius:0;flex-shrink:0;position:relative;text-align:left;min-height:10vh;padding-left:6%}.push-full-screen .push-header .push-avatar{width:5vh;height:5vh;position:absolute;top:2.5vh;left:3vh}.push-full-screen .push-header .push-header-image{width:30vh;height:8vh;position:absolute;left:3vh}.push-full-screen .push-header.push-without-header-avatar .push-title-and-subtitle{margin-left:3%}.push-full-screen .push-title{margin:0;padding-left:0}.push-full-screen .push-subtitle{margin:0;padding-left:0;padding-top:1.5vh;top:50%!important}.push-full-screen .push-close-button,.push-full-screen .push-toggle-fullscreen-button{display:flex;justify-content:center;align-items:center;display:inline-block;background-color:var(--headerBackgroundColor);border:0;width:40px;cursor:pointer}.push-full-screen .push-close-button:active,.push-full-screen .push-close-button:focus,.push-full-screen .push-close-button:hover,.push-full-screen .push-toggle-fullscreen-button:active,.push-full-screen .push-toggle-fullscreen-button:focus,.push-full-screen .push-toggle-fullscreen-button:hover{outline:none}.push-full-screen .push-close-button:active::-moz-focus-inner,.push-full-screen .push-close-button:focus::-moz-focus-inner,.push-full-screen .push-close-button:hover::-moz-focus-inner,.push-full-screen .push-toggle-fullscreen-button:active::-moz-focus-inner,.push-full-screen .push-toggle-fullscreen-button:focus::-moz-focus-inner,.push-full-screen .push-toggle-fullscreen-button:hover::-moz-focus-inner{border:0}.push-full-screen .push-close,.push-full-screen .push-toggle-fullscreen{width:30px;height:30px;padding-top:3px}.push-full-screen .push-close.push-default,.push-full-screen .push-toggle-fullscreen.push-default{width:20px;height:20px}.push-full-screen .push-close.push-default:active,.push-full-screen .push-close.push-default:focus,.push-full-screen .push-close.push-default:hover,.push-full-screen .push-toggle-fullscreen.push-default:active,.push-full-screen .push-toggle-fullscreen.push-default:focus,.push-full-screen .push-toggle-fullscreen.push-default:hover{width:24px;height:24px}.push-full-screen .push-close:active,.push-full-screen .push-close:focus,.push-full-screen .push-close:hover,.push-full-screen .push-toggle-fullscreen:active,.push-full-screen .push-toggle-fullscreen:focus,.push-full-screen .push-toggle-fullscreen:hover{width:34px;height:34px;top:-2px;margin-left:-2px}.push-full-screen .push-loading{border-radius:0;flex-shrink:0;position:relative}@media screen and (max-width:800px){.push-conversation-container .push-header{border-radius:0;flex-shrink:0;position:relative;text-align:left;min-height:5vh!important;padding-left:0!important;text-align:center!important}.push-conversation-container .push-header .push-avatar{display:none}.push-conversation-container .push-header .push-header-image{height:7vh;width:24vh;left:2vh}.push-conversation-container .push-header span.push-subtitle,.push-conversation-container .push-header span.push-with-avatar{text-align:center!important;position:relative;top:3px!important;padding:0;margin:0}.push-conversation-container .push-header.push-without-header-avatar .push-title-and-subtitle{margin-left:0}.push-conversation-container .push-title{margin:0;padding-left:0;text-align:center!important}.push-conversation-container .push-close-button{display:flex;justify-content:center;align-items:center;display:inline-block;background-color:var(--headerBackgroundColor);border:0;width:40px;cursor:pointer}.push-conversation-container .push-close-button:active,.push-conversation-container .push-close-button:focus,.push-conversation-container .push-close-button:hover{outline:none}.push-conversation-container .push-close-button:active::-moz-focus-inner,.push-conversation-container .push-close-button:focus::-moz-focus-inner,.push-conversation-container .push-close-button:hover::-moz-focus-inner{border:0}.push-conversation-container .push-close{width:30px;height:30px;padding-top:3px}.push-conversation-container .push-close.push-default{width:20px;height:20px}.push-conversation-container .push-close.push-default:active,.push-conversation-container .push-close.push-default:focus,.push-conversation-container .push-close.push-default:hover{width:24px;height:24px}.push-conversation-container .push-close:active,.push-conversation-container .push-close:focus,.push-conversation-container .push-close:hover{width:34px;height:34px;top:-2px;margin-left:-2px}.push-conversation-container .push-toggle-fullscreen,.push-conversation-container .push-toggle-fullscreen-button{display:none}.push-conversation-container .push-loading{border-radius:0;flex-shrink:0;position:relative}}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-videoFrame{width:300px;height:200px;margin-top:10px;border:none}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-image-details{object-fit:scale-down;max-width:100%;margin-top:10px}.push-conversation-container .push-image-frame{object-position:0 0;object-fit:cover;width:100%;height:100%;border-radius:15px;cursor:pointer}</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-doc-viewer-modal{position:fixed;top:50%;left:50%;transform:translate(-50%,-50%);z-index:20041997;display:block}.push-doc-viewer-modal-body{overflow-y:auto;border-radius:5px;width:80vw;max-width:900px;position:relative;height:90vh;top:0;background:#fff;animation:push-doc-viewer-slide-down .2s ease}.push-doc-viewer-open-modal-link{color:#003a9b;text-decoration:underline;cursor:pointer;display:inline;background:none!important;border:none;padding:0!important;font:inherit}.push-doc-viewer-modal-iframe{height:100%;width:100%;border:none}.push-doc-viewer-modal-fade{opacity:.5;position:fixed;top:0;right:0;bottom:0;left:0;z-index:10000;background-color:#000;animation:push-appear .2s ease}.push-doc-viewer-modal-footer{flex:0 0 auto;border:none;text-align:center;margin-top:2vh}.push-doc-viewer-close-modal{border-radius:50%;background:#89919b;color:#fff;font-size:15px;width:45px;height:45px;padding:0;text-align:center;cursor:pointer;touch-action:manipulation;border:1px solid transparent;font-weight:100}.push-doc-viewer-spinner{display:inline-block;width:64px;height:64px;position:fixed;top:50%;left:50%;transform:translate(-32px,-32px);animation:push-appear .6s ease-in}.push-doc-viewer-spinner:after{content:" ";display:block;width:46px;height:46px;margin:1px;border-radius:50%;border:5px solid #003a9b;border-color:#003a9b transparent;will-change:transform;animation:push-doc-viewer-spinner 1.2s linear infinite}@keyframes push-doc-viewer-spinner{0%{transform:rotate(0deg)}to{transform:rotate(1turn)}}@keyframes push-appear{0%{opacity:0}to{opacity:.5}}@keyframes push-doc-viewer-slide-down{0%{opacity:0;transform:translateY(-100px)}to{opacity:1;transform:translateY(0)}}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-message{margin:10px;display:flex;font-size:14px;line-height:1.5;font-family:Roboto,serif}.push-conversation-container .push-message .push-markdown p{margin:0 auto}.push-conversation-container .push-client{background-color:var(--userMessageBubbleColor);color:var(--userMessageTextColor);border-radius:15px;padding:11px 15px;text-align:left;font-family:Roboto,serif;margin-left:auto;overflow-wrap:break-word;overflow:auto}.push-conversation-container .push-client a{color:#35cce6}.push-conversation-container .push-response{background-color:var(--botMessageBubbleColor);color:var(--botMessageTextColor);border-radius:0 15px 15px 15px;padding:11px 15px;text-align:left;font-family:Roboto,serif;overflow-wrap:break-word;overflow:auto}.push-conversation-container .push-message-text{margin:0}.push-conversation-container .push-message-text .push-markdown p{margin-bottom:1em}.push-conversation-container .push-message-text .push-markdown p:last-child{margin-bottom:0}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-snippet{background-color:#f4f7f9;color:#000;border-radius:15px;padding:11px 15px;text-align:left;font-family:Roboto,serif}.push-conversation-container .push-snippet-title{margin:0}.push-conversation-container .push-snippet-details{border-left:2px solid #35e65d;margin-top:5px;padding-left:10px}.push-conversation-container .push-link{font-family:Roboto,serif}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-replies{max-width:100%;margin:10px;display:flex;flex-wrap:wrap;font-size:14px;font-family:Roboto,serif}.push-conversation-container .push-reply{display:-webkit-box;display:-webkit-flex;display:-moz-box;display:-ms-flexbox;display:flex;-webkit-flex-align:center;-ms-flex-align:center;-webkit-align-items:center;align-items:center;border:var(--quickRepliesBorderWidth) solid var(--quickRepliesBorderColor);border-radius:20px;min-width:20%;justify-content:center;background-color:var(--quickRepliesBackgroundColor);color:var(--quickRepliesFontColor);padding:4px 8px;min-height:1.9em;text-align:center;font-family:Roboto,serif;cursor:pointer;margin:.25em}.push-conversation-container .push-quickReplies-container{overflow:auto}.push-conversation-container .push-avatar{width:17px;height:17px;border-radius:100%;margin-right:6px;position:relative;bottom:5px}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-group-message.push-from-client span.push-message-date{text-align:right;flex-basis:100%;margin-left:0;margin-right:4px;margin-top:5px;font-size:11px;color:#a4a7ab}.push-group-message.push-from-response span.push-message-date{text-align:left;flex-basis:100%;margin-left:7px;margin-top:5px;font-size:11px;color:#a4a7ab}.push-group-message.push-from-response .push-message:first-child:not(:only-child){margin-bottom:2px}.push-group-message.push-from-response .push-message:first-child:not(:only-child) .push-response{border-radius:0 15px 15px 0}.push-group-message.push-from-response .push-message:not(:first-child):not(:last-child):not(:last-child){margin-top:2px;margin-bottom:2px}.push-group-message.push-from-response .push-message:not(:first-child):not(:last-child):not(:last-child) .push-response{border-radius:0 15px 15px 0}.push-group-message.push-from-response .push-message:not(:first-child):not(:only-child) .push-avatar{display:none}.push-group-message.push-from-response .push-message:not(:first-child):not(:only-child).push-with-avatar{margin-left:49px}.push-group-message.push-from-response .push-message:last-child:not(:only-child){margin-top:2px}.push-message.push-typing-indication .push-response{height:16px}.push-messages-container{background-color:var(--chatBackgroundColor);height:100%;overflow-y:auto;padding-top:10px}.push-messages-container .push-avatar{width:31px;height:31px;margin-top:0;padding-right:2px}.push-widget-embedded .push-messages-container{height:100%;max-height:100%}.push-full-screen .push-messages-container{height:100%;padding:3% 15% 0;max-height:none}.push-full-screen .push-messages-container .push-message{display:flex;font-size:22px;line-height:1.5;font-family:Roboto,serif;font-weight:300}.push-full-screen .push-messages-container .push-message .push-markdown p{margin:0}@keyframes push-typing{0%{width:0}to{width:100%}}.push-full-screen .push-messages-container .push-message .push-response{background-color:var(--fullScreenBotMessageBubbleColor)}.push-full-screen .push-messages-container .push-avatar{width:2.5em;height:2.5em;margin:.5em 0 0 -1em;padding:0}.push-full-screen .push-group-message.push-from-response .push-message:not(:first-child):not(:only-child).push-with-avatar{margin-left:43px}@media screen and (max-width:800px){.push-messages-container{height:100%;padding:3% 15% 0;max-height:none;padding:3% 4% 0!important}.push-messages-container .push-message{display:flex;font-size:22px;line-height:1.5;font-family:Roboto,serif;font-weight:300}.push-messages-container .push-message .push-markdown p{margin:0}@keyframes push-typing{0%{width:0}to{width:100%}}.push-messages-container .push-message .push-response{background-color:var(--fullScreenBotMessageBubbleColor)}.push-messages-container .push-message{font-size:14px!important}.push-messages-container .push-avatar{width:3.5em;height:3.5em;margin:.5em 0 0 -1.5em}.push-group-message.push-from-response .push-message:not(:first-child):not(:only-child).push-with-avatar{margin-left:40px}}div#push-wave{position:relative;width:25px;height:100%;margin-left:auto;margin-right:auto;vertical-align:text-top}div#push-wave .push-customText{display:none;animation:push-wave 1.6s linear infinite;color:#939393}div#push-wave .push-dot{display:inline-block;width:5px;height:5px;border-radius:50%;margin-right:3px;background:#918585;animation:push-wave 1.6s linear infinite}div#push-wave .push-dot:nth-child(2){animation-delay:-1.4s}div#push-wave .push-dot:nth-child(3){animation-delay:-1.2s}@keyframes push-wave{0%,60%,to{transform:none}30%{transform:translateY(-5px)}}</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-sender{align-items:center;display:flex;background-color:var(--inputBackgroundColor);height:45px;padding:5px;margin-bottom:0}.push-conversation-container .push-sender a{display:none}.push-conversation-container .push-new-message{font-size:.9em;width:100%;border:0;background-color:var(--inputBackgroundColor);color:var(--inputFontColor);height:30px;padding-left:15px}.push-conversation-container .push-new-message:focus{outline:none}.push-conversation-container .push-new-message::placeholder{color:var(--inputPlaceholderColor);opacity:1}.push-conversation-container .push-send{background:var(--inputBackgroundColor);border:0;cursor:pointer}.push-conversation-container .push-send:focus{outline:none}.push-conversation-container .push-send .push-send-icon{height:25px;color:#000;fill:red}@media screen and (max-width:800px){.push-conversation-container .push-sender{border-radius:0;flex-shrink:0}}.push-widget-embedded .push-sender{flex:0,0,auto;height:60px}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-conversation-container .push-suggestion{white-space:nowrap;border-style:hidden;color:var(--suggestionsFontColor);min-width:100%;width:auto;max-width:100%;border-radius:0;background-color:transparent;border-bottom:1px solid var(--suggestionsSeparatorColor);position:relative;min-height:1.5em;max-height:1.5em;scroll-snap-align:start;z-index:0;overflow:hidden;align-items:flex-start;text-align:center;padding-top:3px;padding-bottom:3px;cursor:pointer}.push-conversation-container .push-suggestion:hover{color:var(--suggestionsHoverFontColor)}.push-conversation-container .push-suggestions-container{background-color:var(--suggestionsBackgroundColor);border-bottom:1px solid var(--suggestionsSeparatorColor);margin:0;padding-top:5px;display:flex;flex-direction:row;flex-wrap:nowrap}.push-conversation-container .push-suggestions-button{background:transparent;border:0;padding:0;outline:none}.push-conversation-container .push-suggestions-button .push-expand-icon{height:25px;margin-left:5px;padding-right:15px;cursor:pointer}.push-conversation-container .push-suggestions-list{max-width:100%;margin:10px;flex-wrap:wrap;font-size:14px;font-family:Roboto,serif;background-color:var(--suggestionsBackgroundColor);flex-direction:column;flex-wrap:nowrap;margin:1px;width:100%;display:flex}.push-conversation-container .push-hide{visibility:hidden}.push-conversation-container .push-suggestions-list::-webkit-scrollbar{display:none}.push-conversation-container .push-suggestions-list{-ms-overflow-style:none;scrollbar-width:none}.push-conversation-container .push-suggestions-overflow{width:100%;overflow-x:auto;scroll-snap-type:x mandatory;scroll-behavior:smooth}.push-conversation-container .no-border{border:none!important}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}@-webkit-keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@-moz-keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@-webkit-keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@-moz-keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@-webkit-keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@-moz-keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@-webkit-keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}@-moz-keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}@keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}.push-conversation-container{-webkit-animation-delay:0;-webkit-animation-duration:.5s;-webkit-animation-name:push-slide-in;-webkit-animation-fill-mode:forwards;-moz-animation-delay:0;-moz-animation-duration:.5s;-moz-animation-name:push-slide-in;-moz-animation-fill-mode:forwards;animation-delay:0;animation-duration:.5s;animation-name:push-slide-in;animation-fill-mode:forwards;border-radius:10px;box-shadow:0 2px 10px 1px #b5b5b5;overflow:hidden;width:100%;height:100%;display:flex;flex-direction:column}.push-poweredby-container{width:100%;justify-content:center;align-items:flex-end;bottom:0;font:11px Roboto,Arial,Verdana,sans-serif!important;color:#a2a5a8;pointer-events:all;display:flex;padding:5px 0;background-color:#e8eaeb;opacity:inherit}.push-slide-out{-webkit-animation-delay:0;-webkit-animation-duration:.5s;-webkit-animation-name:push-slide-out;-webkit-animation-fill-mode:forwards;-moz-animation-delay:0;-moz-animation-duration:.5s;-moz-animation-name:push-slide-out;-moz-animation-fill-mode:forwards;animation-delay:0;animation-duration:.5s;animation-name:push-slide-out;animation-fill-mode:forwards}.push-widget-embedded .push-conversation-container{-webkit-animation-delay:0;-webkit-animation-duration:0s;-webkit-animation-name:push-slide-in;-webkit-animation-fill-mode:forwards;-moz-animation-delay:0;-moz-animation-duration:0s;-moz-animation-name:push-slide-in;-moz-animation-fill-mode:forwards;animation-delay:0;animation-duration:0s;animation-name:push-slide-in;animation-fill-mode:forwards;display:flex;flex-direction:column;height:100%;width:100%;border-radius:0;overflow:unset}.push-widget-embedded .push-conversation-container .push-sender{align-items:center;display:flex;background-color:var(--inputBackgroundColor);height:15.5vh;padding:0 15%}.push-widget-embedded .push-conversation-container .push-sender a,.push-widget-embedded .push-conversation-container .push-sender a:active,.push-widget-embedded .push-conversation-container .push-sender a:hover{display:inline;position:absolute;bottom:1vh;left:1vh;color:#696969;outline:none;text-decoration:none}.push-widget-embedded .push-conversation-container .push-new-message{font-size:2em;font-weight:300;width:100%;border:0;background-color:var(--inputBackgroundColor);height:10vh;padding-left:15px}.push-widget-embedded .push-conversation-container .push-new-message:focus{outline:none}.push-widget-embedded .push-conversation-container .push-send{background:var(--inputBackgroundColor);border:0}.push-widget-embedded .push-conversation-container .push-send .push-send-icon{height:4.7vh;cursor:pointer}.push-widget-embedded .push-slide-out{-webkit-animation-delay:0;-webkit-animation-duration:0s;-webkit-animation-name:push-slide-out;-webkit-animation-fill-mode:forwards;-moz-animation-delay:0;-moz-animation-duration:0s;-moz-animation-name:push-slide-out;-moz-animation-fill-mode:forwards;animation-delay:0;animation-duration:0s;animation-name:push-slide-out;animation-fill-mode:forwards}.push-full-screen .push-conversation-container{display:flex;flex-direction:column;height:100%;width:100%;border-radius:0}.push-full-screen .push-conversation-container .push-sender{align-items:center;display:flex;background-color:var(--inputBackgroundColor);height:15.5vh;padding:0 15%}.push-full-screen .push-conversation-container .push-sender a,.push-full-screen .push-conversation-container .push-sender a:active,.push-full-screen .push-conversation-container .push-sender a:hover{display:inline;position:absolute;bottom:1vh;left:1vh;color:#696969;outline:none;text-decoration:none}.push-full-screen .push-conversation-container .push-new-message{font-size:2em;font-weight:300;width:100%;border:0;background-color:var(--inputBackgroundColor);height:10vh;padding-left:15px}.push-full-screen .push-conversation-container .push-new-message:focus{outline:none}.push-full-screen .push-conversation-container .push-send{background:var(--inputBackgroundColor);border:0}.push-full-screen .push-conversation-container .push-send .push-send-icon{height:4.7vh;cursor:pointer}.push-full-screen .push-suggestion{font-size:20px}@media screen and (max-width:800px){.push-conversation-container{display:flex;flex-direction:column;height:100%;width:100%;border-radius:0}.push-conversation-container .push-sender{align-items:center;display:flex;background-color:var(--inputBackgroundColor);height:15.5vh;padding:0 15%}.push-conversation-container .push-sender a,.push-conversation-container .push-sender a:active,.push-conversation-container .push-sender a:hover{display:inline;position:absolute;bottom:1vh;left:1vh;color:#696969;outline:none;text-decoration:none}.push-conversation-container .push-new-message{font-size:2em;font-weight:300;width:100%;border:0;background-color:var(--inputBackgroundColor);height:10vh;padding-left:15px}.push-conversation-container .push-new-message:focus{outline:none}.push-conversation-container .push-send{background:var(--inputBackgroundColor);border:0}.push-conversation-container .push-send .push-send-icon{height:4.7vh;cursor:pointer}.push-conversation-container .push-sender{height:10vh!important}.push-conversation-container .push-sender a{font-size:7px!important}.push-conversation-container .push-new-message{font-size:.9em!important}.push-conversation-container .push-send .push-send-icon{height:3vh!important}.push-conversation-container .push-suggestion{font-size:15px}}</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}.push-launcher .push-badge{position:fixed;top:-10px;right:-5px;background-color:red;color:#fff;width:25px;height:25px;text-align:center;line-height:25px;border-radius:50%}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">:root{--titleColor:#fff;--subtitleColor:#fff;--headerBackgroundColor:#003a9b;--launcherColor:#003a9b;--chatBackgroundColor:#fff;--inputBackgroundColor:#f4f7f9;--inputFontColor:#000;--inputPlaceholderColor:#b5b5b5;--userMessageBubbleColor:#003a9b;--userMessageTextColor:#fff;--botMessageBubbleColor:#f4f7f9;--botMessageTextColor:#000;--widgetHeight:65vh;--widgetWidth:370px;--launcherHeight:60px;--launcherWidth:60px;--quickRepliesFontColor:#0084ff;--quickRepliesBackgroundColor:none;--quickRepliesBorderColor:#0084ff;--quickRepliesBorderWidth:1px;--fullScreenBotMessageBubbleColor:transparent;--suggestionsBackgroundColor:#edf1f3;--suggestionsSeparatorColor:#e6e6e6;--suggestionsFontColor:grey;--suggestionsHoverFontColor:#03348a}@-webkit-keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@-moz-keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@-webkit-keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@-moz-keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@-webkit-keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@-moz-keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@-webkit-keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}@-moz-keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}@keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}.push-launcher{-webkit-animation-delay:0;-webkit-animation-duration:.5s;-webkit-animation-name:push-slide-in;-webkit-animation-fill-mode:forwards;-moz-animation-delay:0;-moz-animation-duration:.5s;-moz-animation-name:push-slide-in;-moz-animation-fill-mode:forwards;animation-delay:0;animation-duration:.5s;animation-name:push-slide-in;animation-fill-mode:forwards;display:flex;align-items:center;justify-content:center;background-color:var(--launcherColor);border:0;border-radius:50%;box-shadow:0 2px 10px 1px #b5b5b5;margin-top:10px;min-height:var(--launcherHeight);min-width:var(--launcherWidth);height:var(--launcherHeight);width:var(--launcherWidth);padding:0}.push-launcher:focus{outline:none}.push-launcher.push-hide{display:none}.push-launcher.push-full-screen{margin:0 .5vh .5vh 0}.push-launcher img{width:100%;border-radius:100px;max-height:100%;object-fit:contain}.push-launcher .push-unread-count-pastille{background-color:#e41c23;color:#fff;border-radius:50%;width:15px;height:15px;position:absolute;top:0;right:0;font-weight:700}.push-launcher .push-tooltip-body{background-color:#fff;border-radius:8px;position:absolute;right:78px;bottom:12px;box-shadow:1px 0 13px 3px rgba(0,0,0,.15);width:280px;font-size:1.4em;padding:0 8px;line-height:1.4em;animation:push-slideUp .3s ease-in-out;transition:all .2s ease-in-out;max-width:50vw}.push-launcher .push-tooltip-body:hover{background-color:#fafafa;transform:translateY(-2px);cursor:pointer}.push-launcher .push-tooltip-body:hover .push-tooltip-decoration{background:#fafafa}.push-launcher .push-tooltip-body .push-image-details{object-fit:scale-down;max-width:100%;margin-top:8px;margin-bottom:2px}.push-launcher .push-tooltip-body .push-replies{max-width:100%;margin:10px;display:flex;flex-wrap:wrap;font-size:14px;font-family:Roboto,serif;justify-content:center;margin-left:10px}.push-launcher .push-tooltip-body .push-reply{display:-webkit-box;display:-webkit-flex;display:-moz-box;display:-ms-flexbox;display:flex;-webkit-flex-align:center;-ms-flex-align:center;-webkit-align-items:center;align-items:center;border:var(--quickRepliesBorderWidth) solid var(--quickRepliesBorderColor);border-radius:20px;min-width:20%;justify-content:center;background-color:var(--quickRepliesBackgroundColor);color:var(--quickRepliesFontColor);padding:4px 8px;min-height:1.9em;text-align:center;font-family:Roboto,serif;cursor:pointer;margin:.25em}.push-launcher .push-tooltip-body .push-response{text-align:center;margin-left:10px;margin-right:10px}.push-launcher .push-tooltip-body .push-image-frame{object-position:0 0;object-fit:cover;width:100%;height:100%;border-radius:9px}.push-launcher .push-tooltip-decoration{position:absolute;bottom:12px;right:-6px;background:#fff;height:12px;width:12px;transition:all .2s ease-in-out;clip-path:polygon(100% 50%,50% 0,50% 100%)}.push-launcher .push-tooltip-response{margin-top:1.5rem}.push-launcher .push-tooltip-close{position:absolute;top:2px;right:10px;color:red;height:15px;width:15px;transition:all .2s ease-in-out}.push-launcher .push-tooltip-close button{cursor:pointer;border:none;padding:0}@keyframes push-slideUp{0%{opacity:0;transform:translateY(8px)}to{opacity:1;transform:translateY(0)}}.push-open-launcher{-webkit-animation-delay:0;-webkit-animation-duration:.5s;-webkit-animation-name:push-rotation-rl;-webkit-animation-fill-mode:forwards;-moz-animation-delay:0;-moz-animation-duration:.5s;-moz-animation-name:push-rotation-rl;-moz-animation-fill-mode:forwards;animation-delay:0;animation-duration:.5s;animation-name:push-rotation-rl;animation-fill-mode:forwards}.push-close-launcher{-webkit-animation-delay:0;-webkit-animation-duration:.5s;-webkit-animation-name:push-rotation-lr;-webkit-animation-fill-mode:forwards;-moz-animation-delay:0;-moz-animation-duration:.5s;-moz-animation-name:push-rotation-lr;-moz-animation-fill-mode:forwards;animation-delay:0;animation-duration:.5s;animation-name:push-rotation-lr;animation-fill-mode:forwards}.push-close-launcher.push-default{width:20px!important}@media screen and (max-width:800px){.push-launcher{margin:0 .5vh .5vh 0}.push-hide-sm{display:none}}.push-open-launcher__container{max-height:var(--launcherHeight)}</style>
	<style type="text/css">@import url(https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap);</style>
	<style type="text/css">@-webkit-keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@-moz-keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@keyframes push-rotation-lr{0%{transform:rotate(-90deg)}to{transform:rotate(0)}}@-webkit-keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@-moz-keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@keyframes push-rotation-rl{0%{transform:rotate(90deg)}to{transform:rotate(0)}}@-webkit-keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@-moz-keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@keyframes push-slide-in{0%{opacity:0;transform:translateY(10px)}to{opacity:1;transform:translateY(0)}}@-webkit-keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}@-moz-keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}@keyframes push-slide-out{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(10px)}}.push-widget-container{bottom:0;display:flex;flex-direction:column;padding:0 1vh 1vh 0;position:fixed;right:0;z-index:9999;align-items:flex-end;justify-content:flex-end}.push-widget-container.push-chat-open{width:var(--widgetWidth);height:var(--widgetHeight)}.push-widget-container.push-full-screen.push-chat-open{height:100%;margin:0;padding:0;max-width:none;width:100%}.push-widget-container p{margin-block-start:1em;margin-block-end:1em;margin-inline-start:0;margin-inline-end:0}.push-widget-embedded{box-sizing:border-box;width:100%;height:100%;position:absolute;left:0;right:0;margin:0;z-index:1;display:flex;flex-direction:column}@media screen and (max-width:800px){.push-widget-container.push-chat-open{height:100%;margin:0;padding:0;max-width:none;width:100%}}</style>
	<link rel="stylesheet" type="text/css" href="https://www.ton.com.br/_next/static/css/34cf55f6b4f6a4146c82.css">
	<link rel="stylesheet" type="text/css" href="https://www.ton.com.br/_next/static/css/b7eac1645fde5c2847f3.css">
@include("inc.google-analytics")
</head>
<body>
	<div class="cmp-loader">
	</div>
	<div id="__next">
		<div class="relative">

			<!-- Destaque -->
			<div class="Hero_heroContainer__1McGX Hero_giga__bPV4z">
				<div class="flex flex-col lg:flex-row justify-between items-center py-40 lg:py-64 px-24 md:px-40 m-auto w-full max-w-[1360px] h-full">
					<div class="flex relative flex-col items-center lg:items-start lg:mr-24 mb-24 lg:mb-0">
						<div class="absolute top-[-32px] xl:top-[-32px] xxs:top-[-18px] right-0 xl:right-0 xxs:right-[-24px] p-8 xl:p-12 bg-red-400 rounded-[12px] rounded-bl-sm">
							<p class="font-bold text-white paragraph-12">LANÇAMENTO</p>
						</div>
						<a href="{{ route('ton') }}#maquininhas">
							<h1 class="text-[72px] xl:text-10xl leading-[72px] xl:leading-[96px] text-white whitespace-nowrap">GigaTon</h1>
						</a>
						<p class="Hero_feeText__sAb1G text-[80px] xl:text-[112px] leading-none font-bold text-yellow-300 mt-4 xl:mt-8">7,44%</p>
						<p class="text-4xl xl:text-7xl font-bold leading-[32px] xl:leading-9 text-display-0">No crédito em 12x</p>
						<div class="Hero_priceBallon__2aUSm mt-8 sm:mt-16 bg-display-900 text-display-0 rounded-[40px] rounded-br">
							<p class="paragraph-20 xl:paragraph-24">
								<span class="paragraph-20 xl:paragraph-24">Sua grana cai em</span> <b class="text-ton-500 paragraph-20 xl:paragraph-24">1 dia útil</b>
							</p>
						</div>
					</div>
					<div class="flex relative lg:min-w-[600px] max-w-[320px] sm:max-w-[480px] lg:max-w-[625px]">
						<div class="Hero_machinesTextContainer__DRTeo">
							<a href="{{ route('ton') }}#t3" class="heading-6">
								<svg width="12" height="20" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M10.586 11.414a2 2 0 0 0 0-2.828L3.414 1.414C2.154.154 0 1.047 0 2.828v14.344c0 1.781 2.154 2.674 3.414 1.414l7.172-7.172Z" fill="#FAD400"></path>
								</svg>
								<h6>T3 Giga</h6>
							</a>
							<a href="{{ route('ton') }}#t2" class="heading-6">
								<svg width="12" height="20" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path d="M10.586 11.414a2 2 0 0 0 0-2.828L3.414 1.414C2.154.154 0 1.047 0 2.828v14.344c0 1.781 2.154 2.674 3.414 1.414l7.172-7.172Z" fill="#FAD400"></path>
								</svg>
								<h6>T2 Giga</h6>
							</a>
						</div>
						<div class="flex items-end">
							<a href="{{ route('ton') }}#t2" class="ml-[-16px]">
								<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
									<div style="box-sizing:border-box;display:block;max-width:100%">
										<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTU2IiBoZWlnaHQ9IjI5OCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
									</div>
									<img src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_384,q_auto/site-ton/T2_FRENTE" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
								</div>
							</a>
							<a href="{{ route('ton') }}#t3" class="ml-[-16px]">
								<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
									<div style="box-sizing:border-box;display:block;max-width:100%">
										<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjA3IiBoZWlnaHQ9IjQzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
									</div>
									<img src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_640,q_auto/site-ton/T3_FRENTE" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
								</div>
							</a>
						</div>
						<div class="absolute right-[-16px] sm:right-24 bottom-[-16px] sm:bottom-[10px] lg:bottom-0 lg:left-16 w-[108px] lg:w-[180px] h-[72px] lg:h-[120px]">
							<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
								<div style="box-sizing:border-box;display:block;max-width:100%">
									<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTgwIiBoZWlnaHQ9IjEyMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
								</div>
								<img src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_384,q_auto/site-ton/tag-hero" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
							</div>
						</div>
					</div>
				</div>
				<div class="mb-16 font-normal paragraph-24 text-center hidden xs:block w-full">
					<a href="{{ route('ton') }}#compare-as-taxas" class="mb-40 btn btn-secondary btn-large">Compare as taxas com as das outras máquinas!</a>
				</div>
			</div>

			<div class="flex relative flex-col items-center md:py-80 px-24 md:px-40 pt-24 pb-40 mx-auto">

				<!-- Escolha seu plano -->
				<div class="p-24 xs:p-0 xs:m-0 mx-[-24px] mt-[-24px] xs:w-full xs:rounded-none rounded-b-md shadow xs:shadow-none">
					<h3 class="mb-16 font-bold text-center">Escolha seu plano:</h3>

					<!-- Desktop -->
					<div class="hidden xs:block w-full">
						<div class="flex flex-wrap lg:flex-nowrap gap-24 justify-center w-full ">
							<a href="{{ route('ton') }}#maquininhas" class="w-full max-w-[410px] ">
								<label for="PlansSelect-giga" class="w-full cursor-pointer">
									<div class="p-24 rounded-md border border-ton-500 shadow">
										<div class="flex flex-col mb-16 items-center">
											<div class="flex items-center">
												<input type="radio" name="PlansSelect" id="PlansSelect-giga" class="w-20 h-20" value="giga" checked="">
												<p class="ml-8 font-heading font-bold paragraph-18 md:paragraph-20">GigaTon</p>
											</div>
											<p class="mt-4">Pra você que parcela muito</p>
										</div>
										<div class="p-16 w-full bg-display-100 rounded">
											<ul class="flex">
												<li class="flex-1 text-center">
													<p class="font-bold whitespace-nowrap paragraph-12 xl:paragraph-14">DÉBITO</p>
													<p class="font-heading font-bold text-ton-500 paragraph-20 xl:paragraph-24">1,45<span class="paragraph-16 md:paragraph-18">%</span>
													</p>
												</li>
												<li class="flex-1 text-center">
													<p class="font-bold whitespace-nowrap paragraph-12 xl:paragraph-14">CRÉDITO</p>
													<p class="font-heading font-bold text-ton-500 paragraph-20 xl:paragraph-24">2,90<span class="paragraph-16 md:paragraph-18">%</span>
													</p>
												</li>
												<li class="flex-1 text-center">
													<p class="font-bold whitespace-nowrap paragraph-12 xl:paragraph-14">CRÉDITO 12X</p>
													<p class="font-heading font-bold text-ton-500 paragraph-20 xl:paragraph-24">7,44<span class="paragraph-16 md:paragraph-18">%</span>
													</p>
												</li>
											</ul>
										</div>
									</div>
								</label>
							</a>
						</div>
					</div>

					<!-- Mobile -->
					<div class="xs:hidden w-full">
						<div class="w-full flex justify-center flex-wrap lg:flex-nowrap gap-24 ">
							<a href="{{ route('ton') }}#maquininhas" class="w-full ">
								<label for="PlansSelectWithoutCard-giga" class="w-full cursor-pointer">
									<div class="flex flex-col xl:flex-row items-center mb-16">
										<div class="flex items-center">
											<input type="radio" name="plansSelectWithoutCard" id="PlansSelectWithoutCard-giga" class="w-20 h-20" value="giga" checked="">
											<p class="ml-8 font-heading font-bold paragraph-18">GigaTon</p>
										</div>
										<p class="mt-4 xl:mt-0 xl:ml-auto">Pra você que parcela muito</p>
									</div>
									<div class="p-16 w-full bg-display-100 rounded">
										<ul class="flex gap-4 justify-between">
											<li class="flex-1 text-center">
												<p class="font-bold whitespace-nowrap paragraph-12 xl:paragraph-14">DÉBITO</p>
												<p class="font-heading font-bold text-ton-500 paragraph-20 xl:paragraph-24">1,45<span class="paragraph-16 md:paragraph-18">%</span>
												</p>
											</li>
											<li class="flex-1 text-center">
												<p class="font-bold whitespace-nowrap paragraph-12 xl:paragraph-14">CRÉDITO</p>
												<p class="font-heading font-bold text-ton-500 paragraph-20 xl:paragraph-24">2,90<span class="paragraph-16 md:paragraph-18">%</span>
												</p>
											</li>
											<li class="flex-1 text-center">
												<p class="font-bold whitespace-nowrap paragraph-12 xl:paragraph-14">CRÉDITO 12X</p>
												<p class="font-heading font-bold text-ton-500 paragraph-20 xl:paragraph-24">7,44<span class="paragraph-16 md:paragraph-18">%</span>
												</p>
											</li>
										</ul>
									</div>
								</label>
							</a>
						</div>
					</div>
				</div>

				<!-- Escolha sua maquininha -->
				<h3 class="px-24 mt-40 font-bold text-center">Escolha sua maquininha:</h3>
				<div class="relative w-full">
					<div id="maquininhas" class="anchorV3">
					</div>
					<div class="Machines_machinesContainer__1khGL">
						<!-- T2+ -->
						<div class="flex flex-col w-full max-w-[410px] min-w-[272px] bg-display-0 rounded shadow-new Machines_machine__2RGD8 relative">
							<div class="absolute top-[-100px] md:top-[-188px]" id="t2">
							</div>
							<div class="flex relative p-24 rounded rounded-b-none">
								<div class="absolute top-[-32px] sm:top-[-60px] left-0 max-w-[141px] sm:max-w-[172px] max-h-[176px] sm:max-h-[220px]">
									<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
										<div style="box-sizing: border-box; display: block; max-width: 100%;">
											<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTcyIiBoZWlnaHQ9IjIyMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
										</div>
										<img alt="Imagem da máquininha t2" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_384,q_auto/site-ton/t2" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/t2 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_384,q_auto/site-ton/t2 2x" />
									</div>
								</div>
								<div class="flex justify-end ml-auto">
									<div class="ml-[-22px]">
										<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
											<div style="box-sizing: border-box; display: block; max-width: 100%;">
												<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTYiIGhlaWdodD0iOTYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
											</div>
											<img alt="Icone representando a feature" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/bateria" decoding="async" class="rounded-full" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_96,q_auto/site-ton/bateria 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/bateria 2x" />
										</div>
									</div>
									<div class="ml-[-22px]">
										<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
											<div style="box-sizing: border-box; display: block; max-width: 100%;">
												<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTYiIGhlaWdodD0iOTYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
											</div>
											<img alt="Icone representando a feature" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/comchip" decoding="async" class="rounded-full" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_96,q_auto/site-ton/comchip 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/comchip 2x" />
										</div>
									</div>
								</div>
							</div>
							<div class="px-24">
								<div class="flex justify-between items-center">
									<h2 class="font-bold heading-2">T2+ Giga</h2>
									<div class="flex items-center py-6 px-8 h-[44px] text-red-400 rounded-[12px] rounded-bl-sm border-2 border-red-400">
										<p class="font-heading font-bold paragraph-20">-43%</p>
									</div>
								</div>
								<div class="flex flex-col justify-between">
									<div class="flex mt-16">
										<div class="mr-8 md:mr-16">
											<p class="paragraph-14"><s>R$ 478,80</s></p>
											<p><b><s>R$286,80</s></b></p>
											<p class="paragraph-14">no site oficial,</p>
											<p><b>R$272,46</b></p>
											<p class="paragraph-14">à vista ou</p>
										</div>
										<div class="flex justify-end my-auto ml-auto font-heading font-bold text-ton-500 paragraph-14 md:paragraph-16">
											<div class="self-center mr-4 text-right">
												<p class="leading-none text-display-900">12x</p>
												<p class="mt-4 leading-none text-current">R$</p>
											</div>
											<p class="self-end mt-8 text-[72px] leading-[48px] text-current">22</p>
											<p class="text-current">,71</p>
										</div>
									</div>
								</div>
								<a class="mt-16 w-full font-bold capitalize btn btn-primary btn-large" href="https://sou.ninja/ton-t2-giga" rel="noreferrer">Pedir T2+ Giga</a>
							</div>
							<div class="flex flex-col md:flex-1">
								<div class="flex-1 p-24 mt-auto">
									<ul class="flex flex-col">
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira p-x" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/p-x.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/p-x.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/p-x.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Não precisa de um celular</p>
										</li>
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira r-chat" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/r-chat.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/r-chat.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/r-chat.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Comprovante via aplicativo e SMS</p>
										</li>
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira wifi" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/wifi.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/wifi.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/wifi.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Conexão Wi-Fi e chip</p>
										</li>
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira contactless" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/contactless.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/contactless.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/contactless.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Pagamento por aproximação (NFC)</p>
										</li>
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira battery-charging" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/battery-charging.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/battery-charging.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/battery-charging.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Bateria de longa duração (mais de 12h)</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<!-- T3 -->
						<div class="flex flex-col w-full max-w-[410px] min-w-[272px] bg-display-0 rounded shadow-new Machines_machine__2RGD8 relative">
							<div class="absolute top-[-100px] md:top-[-188px]" id="t3">
							</div>
							<div class="flex relative p-24 rounded rounded-b-none">
								<div class="absolute top-[-32px] sm:top-[-60px] left-0 max-w-[141px] sm:max-w-[172px] max-h-[176px] sm:max-h-[220px]">
									<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
										<div style="box-sizing: border-box; display: block; max-width: 100%;">
											<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTcyIiBoZWlnaHQ9IjIyMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
										</div>
										<img alt="Imagem da máquininha t3" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_384,q_auto/site-ton/t3" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/t3 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_384,q_auto/site-ton/t3 2x" />
									</div>
								</div>
								<div class="flex justify-end ml-auto">
									<div class="ml-[-22px]">
										<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
											<div style="box-sizing: border-box; display: block; max-width: 100%;">
												<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTYiIGhlaWdodD0iOTYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
											</div>
											<img alt="Icone representando a feature" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/combobina" decoding="async" class="rounded-full" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_96,q_auto/site-ton/combobina 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/combobina 2x" />
										</div>
									</div>
									<div class="ml-[-22px]">
										<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
											<div style="box-sizing: border-box; display: block; max-width: 100%;">
												<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iOTYiIGhlaWdodD0iOTYiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
											</div>
											<img alt="Icone representando a feature" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/comchip" decoding="async" class="rounded-full" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_96,q_auto/site-ton/comchip 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_256,q_auto/site-ton/comchip 2x" />
										</div>
									</div>
								</div>
							</div>
							<div class="px-24">
								<div class="flex justify-between items-center">
									<h2 class="font-bold heading-2">T3 Giga</h2>
									<div class="flex items-center py-6 px-8 h-[44px] text-red-400 rounded-[12px] rounded-bl-sm border-2 border-red-400">
										<p class="font-heading font-bold paragraph-20">-59%</p>
									</div>
								</div>
								<div class="flex flex-col justify-between">
									<div class="flex mt-16">
										<div class="mr-8 md:mr-16">
											<p class="paragraph-14"><s>R$ 958,80</s></p>
											<p><b><s>R$406,80</s></b></p>
											<p class="paragraph-14">no site oficial,</p>
											<p><b>R$386,46</b></p>
											<p class="paragraph-14">à vista ou</p>
										</div>
										<div class="flex justify-end my-auto ml-auto font-heading font-bold text-ton-500 paragraph-14 md:paragraph-16">
											<div class="self-center mr-4 text-right">
												<p class="leading-none text-display-900">12x</p>
												<p class="mt-4 leading-none text-current">R$</p>
											</div>
											<p class="self-end mt-8 text-[72px] leading-[48px] text-current">32</p>
											<p class="text-current">,21</p>
										</div>
									</div>
								</div>
								<a class="mt-16 w-full font-bold capitalize btn btn-primary btn-large" href="https://sou.ninja/ton-t3-giga" rel="noreferrer">Pedir T3 Giga</a>
							</div>
							<div class="flex flex-col md:flex-1">
								<div class="flex-1 p-24 mt-auto">
									<ul class="flex flex-col">
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira p-x" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/p-x.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/p-x.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/p-x.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Não precisa de um celular</p>
										</li>
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira receipt-list-43" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/receipt-list-43.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/receipt-list-43.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/receipt-list-43.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Comprovante impresso, via aplicativo ou SMS</p>
										</li>
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira wifi" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/wifi.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/wifi.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/wifi.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Conexão Wi-Fi e chip</p>
										</li>
										<li class="flex mt-16 first:mt-0 list-none first:min-h-40">
											<div class="min-w-[24px] max-w-[24px] min-h-[24px] max-h-[24px]">
												<div style="display: inline-block; max-width: 100%; overflow: hidden; position: relative; box-sizing: border-box; margin: 0px;">
													<div style="box-sizing: border-box; display: block; max-width: 100%;">
														<img style="max-width: 100%; display: block; margin: 0px; border: medium none; padding: 0px;" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
													</div>
													<img alt="Icone Bandeira contactless" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/contactless.svg" decoding="async" style="position: absolute; inset: 0px; box-sizing: border-box; padding: 0px; border: medium none; margin: auto; display: block; width: 0px; height: 0px; min-width: 100%; max-width: 100%; min-height: 100%; max-height: 100%;" srcset="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/site-ton/icons/contactless.svg 1x, https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_48,q_auto/site-ton/icons/contactless.svg 2x" />
												</div>
											</div>
											<p class="ml-12 font-medium paragraph-14">Pagamento por aproximação (NFC)</p>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="flex flex-col items-center py-40 md:py-80 px-24">
						<h3 class="mb-24 font-bold text-center">Aceite as principais bandeiras e vouchers</h3>
						<ul class="flex flex-col items-center md:flex-row gap-16 md:gap-24 ">
							<li class="Flags_itemContainer__36Ets">
								<div>
									<p>Crédito, débito e outros</p>
								</div>
								<ul class="Flags_flagsContainer__1QpGj">
									<li>
										<svg width="48" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M27.034 9.139H19.29v12.898h7.742V9.14Z" fill="#FF5F00"></path>
											<path d="M19.783 15.592a7.697 7.697 0 0 1 .888-3.585 8.34 8.34 0 0 1 2.491-2.863 9.24 9.24 0 0 0-4.523-1.706 9.461 9.461 0 0 0-4.812.778c-1.493.673-2.752 1.721-3.633 3.026a7.766 7.766 0 0 0-1.348 4.351c0 1.54.467 3.048 1.348 4.352.88 1.305 2.14 2.353 3.633 3.026a9.46 9.46 0 0 0 4.812.778 9.241 9.241 0 0 0 4.523-1.706 8.34 8.34 0 0 1-2.492-2.864 7.698 7.698 0 0 1-.887-3.587Z" fill="#EB001B"></path>
											<path d="M36.641 20.673v-.264h.116v-.055h-.293v.055h.116v.264h.061Zm.568 0v-.319h-.088l-.104.228-.103-.228h-.088v.319h.061v-.241l.096.207h.066l.096-.207v.241h.064ZM37.48 15.59c0 1.538-.468 3.047-1.349 4.351-.88 1.305-2.14 2.353-3.633 3.026a9.461 9.461 0 0 1-4.812.778 9.241 9.241 0 0 1-4.524-1.708 8.354 8.354 0 0 0 2.491-2.864 7.713 7.713 0 0 0 .89-3.585 7.713 7.713 0 0 0-.89-3.585 8.354 8.354 0 0 0-2.49-2.864 9.241 9.241 0 0 1 4.523-1.708 9.462 9.462 0 0 1 4.812.778c1.493.673 2.752 1.721 3.633 3.026a7.766 7.766 0 0 1 1.349 4.352v.002Z" fill="#F79E1B"></path>
										</svg>
									</li>
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M20.058 10.09 18.066 21.97h3.185l1.993-11.882h-3.186ZM29.906 9.846c1.235 0 2.224.258 2.855.5l-.432 2.574-.285-.137a5.795 5.795 0 0 0-2.385-.455c-1.247 0-1.824.528-1.824 1.022-.008.557.674.924 1.788 1.473 1.838.848 2.687 1.877 2.675 3.228-.025 2.467-2.199 4.06-5.548 4.06-1.429-.015-2.805-.302-3.549-.634l.447-2.66.41.19c1.047.443 1.725.623 3 .623.916 0 1.898-.364 1.906-1.16.006-.52-.411-.892-1.651-1.474-1.209-.569-2.81-1.521-2.792-3.229.02-2.31 2.237-3.92 5.385-3.92ZM38.498 10.089h2.462l2.578 11.874h-2.956s-.292-1.364-.388-1.78l-2.241-.003-1.839-.002c-.124.321-.67 1.785-.67 1.785h-3.346l4.732-10.889c.334-.774.905-.985 1.668-.985Zm-1.444 7.672h2.644l-.737-3.43-.215-1.025c-.085.235-.203.545-.293.782-.08.214-.139.367-.13.352 0 0-1.005 2.636-1.27 3.321ZM12.242 18.192l3.12-8.103h3.374l-5.015 11.86-3.371.003-2.827-10.3a13.225 13.225 0 0 0-3.37-1.316l.042-.247h5.137c.692.026 1.251.251 1.444 1.004l1.084 5.297c.018.052.034.103.05.155l.332 1.647Z" fill="#1A1F71"></path>
										</svg>
									</li>
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M11.114 12.067a3.978 3.978 0 0 1 5.156 2.984l2.726-.558c-.626-3.088-3.352-5.416-6.624-5.416-.748 0-1.467.122-2.14.348l.882 2.642ZM7.895 20.913l1.844-2.09a3.972 3.972 0 0 1-1.34-2.983 3.96 3.96 0 0 1 1.34-2.978l-1.844-2.084a6.755 6.755 0 0 0-2.28 5.068c0 2.014.882 3.825 2.28 5.067ZM16.27 16.64a3.977 3.977 0 0 1-3.898 3.182c-.44 0-.864-.07-1.264-.203l-.882 2.641a6.76 6.76 0 0 0 8.77-5.062l-2.726-.557ZM28.079 18.51a2.41 2.41 0 0 1-2.987.32l-.905 1.44a4.135 4.135 0 0 0 5.08-.54l-1.188-1.22Zm-1.636-5.869a4.131 4.131 0 0 0-3.52 6.38l7.47-3.198a4.13 4.13 0 0 0-3.95-3.182Zm-2.476 4.39a2.364 2.364 0 0 1-.018-.291 2.43 2.43 0 0 1 2.465-2.392 2.4 2.4 0 0 1 1.816.865l-4.263 1.817Zm8.775-6.247v7.97l1.38.575-.655 1.573-1.369-.569a1.528 1.528 0 0 1-.673-.569c-.15-.232-.266-.557-.266-.986v-7.994h1.583Zm4.982 3.686c.244-.081.5-.122.772-.122 1.177 0 2.151.836 2.378 1.945l1.664-.343a4.126 4.126 0 0 0-5.347-3.094l.533 1.614Zm-1.966 5.393 1.125-1.271a2.423 2.423 0 0 1-.818-1.823c0-.726.32-1.376.818-1.817l-1.125-1.271a4.126 4.126 0 0 0 0 6.182Zm5.116-2.6a2.432 2.432 0 0 1-2.378 1.944c-.267 0-.528-.046-.772-.128l-.54 1.614a4.126 4.126 0 0 0 5.353-3.088l-1.663-.343Z" fill="#000"></path>
										</svg>
									</li>
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="m33.68 10.899-.762 1.69h1.528l-.766-1.69Zm-8.793 1.048c.143-.069.228-.218.228-.404 0-.181-.09-.313-.233-.375-.13-.068-.332-.076-.525-.076h-1.363v.942h1.345c.215 0 .395-.003.548-.087Zm-17.47-1.048-.753 1.69h1.511l-.758-1.69ZM42.495 21.73h-2.138v-.91h2.13c.21 0 .359-.025.448-.104a.36.36 0 0 0 .13-.277.336.336 0 0 0-.135-.28c-.08-.065-.197-.095-.39-.095-1.04-.032-2.336.03-2.336-1.313 0-.615.426-1.263 1.588-1.263h2.206v-.844h-2.05c-.618 0-1.067.136-1.385.347v-.347h-3.03c-.486 0-1.054.11-1.323.347v-.347h-5.413v.347c-.43-.285-1.157-.347-1.493-.347h-3.57v.347c-.34-.302-1.098-.347-1.56-.347h-3.996l-.914.906-.856-.906h-5.969v5.92h5.856l.942-.92.888.92 3.61.003v-1.393h.354c.48.007 1.044-.01 1.542-.208v1.598h2.978V21.02h.143c.184 0 .202.007.202.175v1.367h9.044c.574 0 1.175-.134 1.507-.378v.379h2.869c.597 0 1.18-.077 1.623-.273v-1.103c-.268.36-.793.543-1.502.543Zm-18.358-1.397h-1.382v1.423h-2.152L19.24 20.35l-1.417 1.405h-4.386v-4.243h4.454l1.362 1.39 1.408-1.39h3.538c.878 0 1.866.223 1.866 1.398 0 1.178-.96 1.422-1.928 1.422Zm6.643-.193c.156.207.179.4.183.775v.84h-1.111v-.53c0-.255.026-.633-.18-.83-.161-.152-.408-.189-.812-.189h-1.183v1.55h-1.112v-4.243h2.556c.56 0 .969.023 1.332.2.35.194.57.459.57.943-.001.677-.494 1.022-.786 1.128.247.083.448.233.543.356Zm4.573-1.75H32.76v.772h2.53v.865h-2.53v.845l2.593.003v.88h-3.695v-4.242h3.695v.878Zm2.845 3.365h-2.156v-.91h2.148c.21 0 .359-.025.452-.104a.363.363 0 0 0 .13-.277.352.352 0 0 0-.134-.28c-.085-.065-.202-.095-.395-.095-1.035-.032-2.332.03-2.332-1.313 0-.615.422-1.263 1.583-1.263h2.22v.903h-2.031c-.202 0-.332.007-.444.077-.121.069-.166.17-.166.305 0 .16.103.269.242.316.117.037.242.048.431.048l.596.015c.6.013 1.014.109 1.264.341.216.204.332.462.332.899 0 .912-.623 1.338-1.74 1.338Zm-8.587-3.296c-.134-.073-.331-.076-.528-.076H27.72v.952h1.345c.215 0 .398-.007.546-.087a.448.448 0 0 0 .23-.407.398.398 0 0 0-.23-.382Zm12.37-.077c-.2 0-.335.007-.447.077-.117.069-.162.17-.162.305 0 .16.099.27.242.316.117.038.242.048.427.048l.6.015c.605.014 1.01.11 1.256.342.045.032.072.068.103.105v-1.207h-2.018Zm-17.806 0h-1.444v1.08h1.431c.425 0 .69-.192.69-.56 0-.37-.277-.52-.677-.52Zm-9.635 0v.772h2.43v.865h-2.43v.844h2.722l1.265-1.244-1.211-1.237H14.54Zm7.108 2.967v-3.401l-1.7 1.673 1.7 1.728Zm-7.015-6.811v.73h9.25l-.004-1.545h.179c.125.004.162.014.162.204v1.342h4.784v-.36c.386.19.986.36 1.776.36h2.012l.431-.942h.955l.421.942h3.879v-.895l.587.895h3.108V9.354h-3.076v.699l-.43-.699H35.51v.699l-.395-.699H30.85c-.713 0-1.34.091-1.847.345v-.345h-2.942v.345c-.323-.262-.762-.345-1.25-.345h-10.75l-.72 1.528-.741-1.528H9.215v.699l-.372-.699H5.956l-1.34 2.812v2.25l1.982-4.241h1.645l1.883 4.015v-4.015h1.807l1.449 2.877 1.331-2.877h1.844v4.241h-1.135l-.005-3.322-1.605 3.322h-.972l-1.61-3.325v3.325H8.978l-.425-.946H6.248l-.43.946H4.615v.853h1.892l.427-.942h.955l.425.942h3.722v-.72l.332.723H14.3l.333-.734Zm14.576-3.869c.358-.338.919-.494 1.682-.494h1.071v.908h-1.049c-.404 0-.632.055-.852.252-.189.178-.318.516-.318.96 0 .455.099.782.304.996.17.168.48.219.772.219h.497l1.56-3.335h1.66l1.874 4.011v-4.011h1.686l1.946 2.954v-2.954h1.134v4.24h-1.569l-2.098-3.182v3.183h-2.255l-.431-.946h-2.3l-.418.946h-1.296c-.538 0-1.22-.11-1.605-.47-.39-.36-.591-.847-.591-1.618 0-.63.12-1.204.596-1.659Zm-2.277-.494h1.13v4.24h-1.13v-4.24Zm-5.093 0h2.546c.566 0 .983.013 1.34.203.351.19.561.466.561.939a1.2 1.2 0 0 1-.78 1.13c.242.085.45.234.548.358.156.21.183.399.183.778v.833H25.12l-.004-.535c0-.255.027-.622-.174-.826-.162-.15-.409-.182-.808-.182h-1.188v1.543h-1.107v-4.241Zm-4.464 0h3.699v.883h-2.591v.764h2.529v.87h-2.53v.847h2.592v.877h-3.699v-4.241Z" fill="#3285D2"></path>
										</svg>
									</li>
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M14.986 6.68h-4.147c-1.828.088-3.329.833-3.76 2.372-.22.802-.344 1.682-.527 2.515C5.657 15.795 4.864 20.134 4 24.275h32.273c2.497 0 4.21-.532 4.672-2.531.212-.928.424-1.983.629-3.006.8-3.983 1.6-7.966 2.426-12.058H14.986Z" fill="#B82327"></path>
											<path d="M14.664 12.931c.171-.118.4-.666.14-.896a.488.488 0 0 0-.408-.063.502.502 0 0 0-.343.182c-.11.15-.212.61-.04.777a.619.619 0 0 0 .65 0Zm-2.466-.896c-.126.817-.267 1.61-.409 2.411-.902.008-1.821.047-2.693-.024.165-.777.283-1.61.448-2.387h-.966c-.343 1.99-.668 4.014-1.052 5.965h.989c.157-.999.298-2.014.502-2.959.89-.043 1.781-.036 2.67.024-.165.992-.37 1.944-.526 2.944h.989c.322-2.02.652-4.022 1.052-5.965h-1.004v-.009Zm14.017 1.713a1.317 1.317 0 0 0-.967.03 1.336 1.336 0 0 0-.69.684c.062-.222.086-.483.14-.714h-.769c-.188 1.476-.463 2.856-.73 4.252h.871a16.85 16.85 0 0 1 .448-2.736 1.233 1.233 0 0 1 .601-.803 1.206 1.206 0 0 1 .993-.078c.008-.23.079-.412.103-.635ZM26.7 17a2.117 2.117 0 0 1-.078-.816c.025-.593.23-1.164.59-1.635.456-.451 1.35-.372 2.064-.118.024-.246.071-.462.103-.698-1.17-.19-2.286-.072-2.874.555a3.52 3.52 0 0 0-.694 2.903c.314 1.031 1.714 1.097 2.85.698.047-.206.078-.436.118-.65-.618.309-1.804.476-2.079-.239Zm9.76-3.228c-.769-.389-1.405.261-1.656.65.07-.198.078-.468.14-.674h-.769A64.127 64.127 0 0 1 33.431 18h.887a9.962 9.962 0 0 1 .204-1.53c.189-1.183.457-2.475 1.822-2.087.047-.198.063-.429.118-.611Zm-22.685-.024c-.023 0-.023.032-.023.064a56.667 56.667 0 0 1-.73 4.188h.87a61.41 61.41 0 0 1 .747-4.252h-.864Zm7.64-.079a2.245 2.245 0 0 0-1.538.635 3.571 3.571 0 0 0-.73 2.578c.157 1.412 1.9 1.365 3.297 1.024.024-.246.087-.462.118-.698-.571.214-1.57.523-2.167.142-.448-.285-.448-1.007-.307-1.634a48.98 48.98 0 0 1 2.85 0c.132-.445.159-.915.079-1.372-.178-.58-.879-.73-1.601-.675Zm.833 1.555h-2.041c.043-.28.183-.536.393-.723.211-.187.48-.293.76-.3.622-.024 1.08.23.888 1.023Zm-4.122-1.476a2.262 2.262 0 0 0-2.018.373c0 .016-.008.016-.024.016.008-.008.016-.008.024-.016.008-.135.057-.23.063-.365h-.747a161.2 161.2 0 0 1-1.068 6.148h.857c.126-.785.213-1.61.385-2.348.197.777 1.492.627 2.042.325 1.13-.618 1.994-3.554.486-4.133Zm-.69 3.451c-.464.5-1.61.492-1.697-.346.002-.382.058-.76.165-1.127.071-.38.118-.745.18-1.087.458-.563 1.807-.634 1.94.31a3.14 3.14 0 0 1-.59 2.25h.001Zm22.268-5.291c-.068.64-.17 1.276-.306 1.905-2.136-.682-3.44.904-3.416 2.864-.021.367.087.73.306 1.023a1.956 1.956 0 0 0 2.144.183c.115-.09.218-.193.306-.31.057-.071.15-.261.165-.206-.043.21-.07.422-.078.635h.785a54.04 54.04 0 0 1 .972-6.084h-.88l.002-.01Zm-1.948 5.624c-.589.016-.88-.357-.887-.96-.023-1.063.44-2.245 1.371-2.348.367-.032.736.025 1.076.167-.295 1.19-.185 3.11-1.56 3.141Zm-7.498-3.784c-.04.246-.11.462-.166.698.51-.127 2.115-.531 2.262.167.029.222-.007.448-.102.65-1.445-.134-2.623.104-2.93 1.143a1.304 1.304 0 0 0 .464 1.57c.392.155.823.176 1.228.061a1.942 1.942 0 0 0 1.018-.695 2.767 2.767 0 0 0-.063.65h.746a11.85 11.85 0 0 1 .22-1.959 8.12 8.12 0 0 0 .243-1.594c-.053-1.12-1.892-.723-2.92-.69Zm1.413 3.435c-.447.445-1.704.57-1.578-.492.11-.88 1.052-1.063 2.081-.936a2.57 2.57 0 0 1-.503 1.428Z" fill="#fff"></path>
										</svg>
									</li>
								</ul>
							</li>
							<li class="Flags_itemContainer__36Ets">
								<div>
									<p>
										<span>Voucher <b>(em breve)</b>
										</span>
									</p>
								</div>
								<ul class="Flags_flagsContainer__1QpGj">
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M29.788 8.21c3.415 0 5.186 1.753 5.186 4.512 0 1.55-.73 2.957-2.433 3.774.715 2.046 1.431 4.023 2.351 6.068H29.37l-2.444-8.134c.223.052.45.08.678.086.97 0 2.316-.101 2.316-1.28 0-1.012-.753-1.314-1.852-1.314H26.85c-1.543 3.545-3.006 7.098-4.459 10.638h-5.89c-1.685-4.563-2.089-5.884-4.091-10.662h5.248l1.875 6.163h.043l3.264-9.856 6.948.004Z" fill="#00A513"></path>
										</svg>
									</li>
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M29.714 5.539c-2.127 0-4.206.61-5.974 1.755-.122.075-.968.653-.998.685-1.398 1.009-2.335 1.19-3.61.878-.076-.015-.445-.15-.568-.178a8.219 8.219 0 0 0-6.743.522 7.901 7.901 0 0 0-2.568 2.254 7.612 7.612 0 0 0-1.342 3.096 7.506 7.506 0 0 0 .139 3.355A7.657 7.657 0 0 0 9.64 20.89a7.96 7.96 0 0 0 2.746 2.048 8.197 8.197 0 0 0 3.382.729 7.818 7.818 0 0 0 2.52-.402c1.903-.551 2.948-.402 4.145.415.245.164.814.522.937.61a10.9 10.9 0 0 0 4.619 1.883c1.672.265 3.384.142 4.997-.36 1.613-.5 3.08-1.364 4.283-2.52a10.337 10.337 0 0 0 2.63-4.134 10.08 10.08 0 0 0 .405-4.84 10.212 10.212 0 0 0-1.91-4.49c-.995-1.328-2.3-2.41-3.809-3.156a10.993 10.993 0 0 0-4.872-1.133Z" fill="#00765E"></path>
											<path d="M25.861 17.534c-.18.17-.394.305-.629.394-.235.09-.485.132-.738.126a1.804 1.804 0 0 1-.98-.282l-.709 1.086c.5.308 1.081.473 1.674.475.428.007.854-.068 1.252-.22.397-.154.76-.381 1.067-.67l-.937-.91ZM24.571 13.07a3.297 3.297 0 0 0-2.302.89 3.095 3.095 0 0 0-.981 2.205 3.007 3.007 0 0 0 .521 1.756l5.866-2.426a3.13 3.13 0 0 0-1.121-1.73 3.304 3.304 0 0 0-1.983-.695Zm-1.936 3.333a1.091 1.091 0 0 1-.015-.223c.002-.243.055-.483.154-.706.099-.223.242-.425.422-.594.18-.168.393-.301.626-.39a1.948 1.948 0 0 1 1.517.056c.246.113.465.273.644.47l-3.348 1.387ZM29.517 11.655v6.055l1.09.431-.507 1.191-1.077-.429a1.152 1.152 0 0 1-.522-.431 1.272 1.272 0 0 1-.215-.745v-6.072h1.23ZM20.072 11.655v6.055l1.09.431-.506 1.191-1.078-.429a1.152 1.152 0 0 1-.521-.431 1.27 1.27 0 0 1-.214-.745v-6.072h1.23Z" fill="#fff"></path>
											<path d="M34.846 14.542c.226.105.428.253.596.434.167.181.296.392.378.622a1.795 1.795 0 0 1-.084 1.415l1.198.565a3.05 3.05 0 0 0 .224-2.108 3.135 3.135 0 0 0-1.21-1.765 3.31 3.31 0 0 0-2.093-.616 3.291 3.291 0 0 0-2.02.813l.891.95c.28-.254.632-.42 1.01-.475.379-.055.766.003 1.11.165Z" fill="#CAD459"></path>
											<path d="M33.202 17.876a1.902 1.902 0 0 1-.596-.434 1.837 1.837 0 0 1-.379-.622 1.794 1.794 0 0 1 .085-1.415l-1.198-.566a3.049 3.049 0 0 0-.223 2.108c.18.706.608 1.33 1.21 1.764a3.31 3.31 0 0 0 2.092.617 3.292 3.292 0 0 0 2.02-.812l-.891-.95a1.972 1.972 0 0 1-2.12.31ZM17.615 17.966v-1.771a3.063 3.063 0 0 0-.627-1.846 3.226 3.226 0 0 0-1.63-1.13 3.435 3.435 0 0 0-1.951-.015c-.5.147-.953.412-1.32.771a3.097 3.097 0 0 0-.89 1.6 3.036 3.036 0 0 0 .18 1.809 3.148 3.148 0 0 0 1.188 1.406c.531.344 1.156.528 1.794.528.501 0 .996-.111 1.444-.328l-.49-1.161c-.281.15-.598.227-.919.223-.25 0-.498-.047-.729-.14a1.91 1.91 0 0 1-.617-.4 1.841 1.841 0 0 1-.413-.599 1.795 1.795 0 0 1-.145-.706c-.004-.303.075-.6.23-.864.131-.232.311-.435.528-.597.217-.161.466-.277.732-.34a2.003 2.003 0 0 1 1.212.119c.333.147.615.384.814.682.198.298.305.645.307 1v1.74a1.237 1.237 0 0 0 .736 1.177l.6.237.506-1.19-.54-.205Z" fill="#fff"></path>
										</svg>
									</li>
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M37.599 11.988c.247-.455.48-.917.733-1.372.067.466.139.927.211 1.394.5-.152.987-.331 1.492-.461-.355.39-.698.792-1.04 1.193.373.267.745.52 1.112.786-.5.05-.999.071-1.498.109-.018.477-.054.96-.084 1.443-.289-.348-.553-.705-.83-1.058-.416.342-.836.684-1.24 1.036.157-.49.314-.976.464-1.465-.481-.054-.968-.114-1.45-.173.47-.24.945-.484 1.414-.728-.192-.417-.39-.83-.577-1.248.427.19.866.353 1.294.543M20.688 21.167c-.083 0-.17 0-.255-.005a2.822 2.822 0 0 1-1.03-.2 2.586 2.586 0 0 1-.853-.554 1.982 1.982 0 0 1-.393-.926 1.907 1.907 0 0 1 .122-.985c.118-.383.333-.736.63-1.03.295-.297.66-.53 1.067-.684a5.471 5.471 0 0 1 2.086-.326c.262 0 .526.008.782.016h.005l.221.006c.072-.343.149-.692.221-1.029v-.006l.162-.75c.274-.021.55-.037.818-.054h.003l.437-.027c-.27 1.3-.587 2.811-.909 4.318-.115.673-.503 1.284-1.09 1.714a3.553 3.553 0 0 1-2.024.522Zm1.4-3.956a3.502 3.502 0 0 0-1.499.287c-.255.126-.478.3-.652.51-.174.21-.297.45-.358.707-.14.36-.13.753.03 1.107.09.14.22.257.374.34.155.084.33.13.51.137.1.011.201.017.302.017.416.014.824-.111 1.142-.353.298-.266.486-.614.536-.989.056-.242.11-.488.164-.726.073-.326.148-.663.227-.993a7.8 7.8 0 0 0-.775-.044Z" fill="#283C93"></path>
											<path d="M32.96 18.27a29.693 29.693 0 0 0 3.49-3.229c-.491 1.522-1.426 2.897-2.708 3.982a8.908 8.908 0 0 1-.915.657 7.355 7.355 0 0 1-3.736 1.117l.03-.021a23.585 23.585 0 0 0 3.008-1.862c.277-.21.572-.412.83-.645" fill="#EA2228"></path>
											<path d="M13.925 21.214a3.33 3.33 0 0 1-1.607-.347 1.71 1.71 0 0 1-.592-.51 1.5 1.5 0 0 1-.275-.694 2.56 2.56 0 0 1 .686-1.975c.632-.664 1.528-1.08 2.497-1.155.173-.017.346-.025.52-.025a3.352 3.352 0 0 1 1.574.339c.233.121.432.288.583.488.151.2.25.428.29.667a2.505 2.505 0 0 1-.53 1.85c-.315.389-.721.71-1.19.943-.467.233-.986.37-1.52.402-.148.011-.297.017-.436.017Zm1.037-3.924a2.19 2.19 0 0 0-.454.049 2.35 2.35 0 0 0-1.258.8c-.307.386-.46.854-.433 1.329.006.225.102.44.27.607.13.107.284.189.45.242.166.053.343.076.52.067h.07c.309-.003.613-.069.89-.192s.52-.3.712-.518c.278-.326.45-.715.5-1.123.03-.18.015-.363-.048-.536a1.13 1.13 0 0 0-.314-.457 1.504 1.504 0 0 0-.905-.265v-.003ZM27.07 21.331c-.23 0-.46-.017-.686-.052a2.535 2.535 0 0 1-1.6-.786 1.6 1.6 0 0 1-.397-1.237c.034-.713.377-1.386.957-1.873.73-.596 1.691-.913 2.678-.884h.113a2.62 2.62 0 0 1 1.522.407.97.97 0 0 1 .362.629 1.14 1.14 0 0 1-.121.706c-.237.36-.608.633-1.047.771a5.35 5.35 0 0 1-1.72.26 8.94 8.94 0 0 1-1.433-.124c-.037.26.01.526.138.764.127.237.327.436.578.573.544.243 1.148.36 1.756.34.317 0 .635-.018.95-.05l-.03.022a3.84 3.84 0 0 1-2.02.534Zm.833-4.123a2.495 2.495 0 0 0-.93.188 2.152 2.152 0 0 0-1.095 1.057c.383.062.77.094 1.16.097.39.005.779-.047 1.15-.153a.807.807 0 0 0 .438-.26.67.67 0 0 0 .163-.449.468.468 0 0 0-.137-.276.561.561 0 0 0-.29-.152 2.02 2.02 0 0 0-.459-.052ZM38.38 21.267a3.72 3.72 0 0 1-1.461-.27 1.819 1.819 0 0 1-.659-.462 1.587 1.587 0 0 1-.362-.677 2.398 2.398 0 0 1 .37-1.79c.281-.415.66-.769 1.113-1.035a3.868 3.868 0 0 1 1.499-.5c.247-.033.495-.05.745-.05.49-.01.975.08 1.421.261.27.108.508.273.691.482.184.21.308.455.361.717a2.466 2.466 0 0 1-.475 1.873c-.293.385-.673.71-1.116.953a3.94 3.94 0 0 1-1.453.456c-.223.028-.449.042-.675.042Zm1.065-3.982c-.154 0-.307.015-.457.044a2.372 2.372 0 0 0-1.317.841c-.316.41-.46.908-.41 1.406.015.16.078.312.183.441.105.13.248.23.412.29.222.08.459.12.698.119.279-.002.554-.05.813-.145a2.256 2.256 0 0 0 1.006-.79c.242-.346.365-.749.354-1.158a.95.95 0 0 0-.113-.459 1.064 1.064 0 0 0-.326-.366 1.52 1.52 0 0 0-.843-.223ZM7.961 16.739a6.916 6.916 0 0 1 1.703-.184c.633-.009 1.265.035 1.889.13-.09.217-.18.428-.259.646a10.1 10.1 0 0 0-2.713.07c-.27.065-.62.163-.693.45-.012.218.21.342.415.396.638.158 1.323.134 1.925.391a.943.943 0 0 1 .372.245c.1.108.168.236.2.374.04.223.025.45-.045.667-.07.217-.192.417-.359.586a2.833 2.833 0 0 1-1.516.64 9.408 9.408 0 0 1-3.42-.178c.061-.228.133-.456.194-.684a9.772 9.772 0 0 0 2.894.065c.36-.07.77-.244.836-.608.072-.239-.162-.428-.39-.49-.608-.168-1.259-.13-1.854-.33a1.017 1.017 0 0 1-.362-.183.897.897 0 0 1-.246-.301c-.08-.21-.09-.435-.029-.65a1.11 1.11 0 0 1 .376-.554 2.871 2.871 0 0 1 1.077-.499M30.271 16.555c.391 0 .788.005 1.18 0 .48.586.998 1.15 1.54 1.687l-.03.027c-.26.233-.554.434-.831.645a23.366 23.366 0 0 1-1.86-2.36M32.827 19.68c.32-.202.626-.42.915-.657.74.732 1.492 1.453 2.244 2.175-.524-.011-1.053.005-1.576-.006a2.075 2.075 0 0 1-.343-.287 17.684 17.684 0 0 1-1.24-1.226" fill="#283C93"></path>
										</svg>
									</li>
									<li>
										<svg width="49" height="32" fill="none" xmlns="http://www.w3.org/2000/svg">
											<path d="M31.443 15.927H29.45a1.05 1.05 0 0 1 .997-.841.932.932 0 0 1 .997.841Zm3.768 2.756a11.077 11.077 0 0 1-21.628-4.777h1.114v4.789h1.408v-4.79h1.71l.166-1.211h-4.094a11.08 11.08 0 0 1 21.343 5.905 3.832 3.832 0 0 1-.797-2.287v-.983h.716l.18-.99h-.897v-1.602l-1.307.554v2.984a3.82 3.82 0 0 0 .754 2.407l1.332.001Zm-15.62-4.43h-1.33v4.442h1.33v-4.443Zm0-1.559h-1.33v.99h1.33v-.99Zm1.777 3.782a1.123 1.123 0 0 1 1.242-1.233c.242.016.476.087.686.207l.37-.987a2.674 2.674 0 0 0-1.311-.3 2.312 2.312 0 1 0 0 4.623c.339 0 .674-.071.983-.21l.113-1.073a1.573 1.573 0 0 1-.84.207 1.174 1.174 0 0 1-1.243-1.236v.002Zm6.811 2.22a9.125 9.125 0 0 1-.425-1.081 4.534 4.534 0 0 0-.495-1.307.945.945 0 0 0-.694-.222l1.448-1.83h-1.436l-1.23 1.756h-.144v-3.319h-1.292v5.998h1.292v-1.837h.54c.61 0 .559.51.757 1.073.162.464.29.762.29.762l1.39.007Zm4.65-2.221a2.214 2.214 0 0 0-2.386-2.313 2.24 2.24 0 0 0-2.387 2.312c0 1.346.987 2.311 2.642 2.311.398-.005.793-.08 1.166-.22l.089-1.011a1.95 1.95 0 0 1-1.071.251 1.29 1.29 0 0 1-1.388-1.011h3.335v-.32Z" fill="#E80000"></path>
										</svg>
									</li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="mx-auto w-full max-w-[1280px] mb-40">
						<div id="taxas" class="md:top-[-160px] anchor">
						</div>
						<div class="TonFeeCard_container__2JG5x">
							<div class="TonFeeCard_titleContainer__1o37R">
								<h3 class="font-bold heading-3">E quais são as taxas do GigaTon?</h3>
								<div class="flex flex-col md:flex-row items-center mt-8 md:mt-0 paragraph-16">
									<p>Taxas para vendas das bandeiras <strong>Mastercard e Visa</strong></p>
								</div>
							</div>
							<ul class="TonFeeCard_feesList__38LzW">
								<li>
									<p class="font-semibold">DÉBITO</p>
									<h3 class="py-8 text-ton-500">1,45%</h3>
									<p>Recebimento em <b>1 dia útil</b>
									</p>
								</li>
								<li>
									<p class="font-semibold">CRÉDITO À VISTA</p>
									<h3 class="py-8 text-ton-500">2,90%</h3>
									<p>Recebimento em <b>1 dia útil</b>
									</li>
									<li>
										<p class="font-semibold">CRÉDITO PARCELADO 12x</p>
										<h3 class="py-8 text-ton-500">7,44%</h3>
										<p>Recebimento em <b>1 dia útil</b>
										</li>
									</ul>
								</div>
							</div>
							<div class="mx-auto w-full max-w-[1280px]">
								<div class="md:top-[-160px] anchor">
								</div>
								<div class="TonFeeCard_container__2JG5x">
									<div class="TonFeeCard_titleContainer__1o37R">
										<div class="flex flex-col md:flex-row items-center mt-8 md:mt-0 paragraph-16">
											<p>Taxas para vendas das bandeiras <strong>Elo, Amex e Hipercard</strong></p>
										</div>
									</div>
									<ul class="TonFeeCard_feesList__38LzW">
										<li>
											<p class="font-semibold">DÉBITO</p>
											<h3 class="py-8 text-ton-500">2,45%</h3>
											<p>Recebimento em <b>1 dia útil</b>
											</p>
										</li>
										<li>
											<p class="font-semibold">CRÉDITO À VISTA</p>
											<h3 class="py-8 text-ton-500">4,09%</h3>
											<p>Recebimento em <b>1 dia útil</b>
											</li>
											<li>
												<p class="font-semibold">CRÉDITO PARCELADO 12x</p>
												<h3 class="py-8 text-ton-500">8,50%</h3>
												<p>Recebimento em <b>1 dia útil</b>
												</li>
											</ul>
											<div class="mt-24 w-full text-center">
												<a class="paragraph-14" href="https://registon.api.ton.com.br/documents/regulamento-gigaton/latest" target="_blank" rel="noreferrer">
													*Conheça o <u>regulamento para o GigaTon</u>
												</a>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="flex flex-col items-center py-40 lg:py-[80px] px-0 sm:px-24 md:px-40 bg-display-50">
								<h2 class="font-bold text-center heading-2">Descubra o que Ton pode te oferecer</h2>
								<div class="flex flex-col justify-center items-center">
									<div class="Offers_offer__PE9kl">
										<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
											<div style="box-sizing:border-box;display:block;max-width:100%">
												<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDAwIiBoZWlnaHQ9IjQwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
											</div>
											<img alt="Cartão da Ton" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_640,q_auto/site-ton/ton-cartao" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
										</div>
										<div>
											<h3>Cartão grátis</h3>
											<h4>Pra você usar o seu dinheiro</h4>
											<ul>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Transfira o dinheiro das suas vendas para o seu cartão pré-pago" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Transfira o dinheiro das suas vendas para o seu cartão pré-pago</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Faça compras físicas, online e até internacionais" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Faça compras físicas, online e até internacionais</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Saque o seu dinheiro na rede Banco24H" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Saque o seu dinheiro na rede Banco24H</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Solicite direto pelo Aplicativo Ton!" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Solicite direto pelo Aplicativo Ton!</p>
												</li>
												<div class="mt-16">
													<p class="font-bold paragraph-16">* Disponível apenas para cadastros feitos no CPF</p>
												</div>
												<a href="https://play.google.com/store/apps/details?id=br.com.stone.ton" class="mt-24 btn btn-secondary btn-large">Baixe o app para Android</a>
												<a href="https://apps.apple.com/us/app/ton/id1496404455?l=pt&ls=1" class="mt-24 btn btn-secondary btn-large">Baixe o app para iPhone</a>
											</ul>
										</div>
									</div>
									<div class="Offers_offer__PE9kl">
										<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
											<div style="box-sizing:border-box;display:block;max-width:100%">
												<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDAwIiBoZWlnaHQ9IjQwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
											</div>
											<img alt="Aplicativo da Ton" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_640,q_auto/site-ton/ton-aplicativo" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
										</div>
										<div>
											<h3>Aplicativo Ton</h3>
											<h4>Pra você gerenciar</h4>
											<ul>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Acesse sua conta onde e quando quiser" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Acesse sua conta onde e quando quiser</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Crie o seu catálogo de produtos para facilitar sua rotina de vendas" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Crie o seu catálogo de produtos para facilitar sua rotina de vendas</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Acompanhe suas vendas e crie metas que te ajudem a alavancar o seu negócio" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Acompanhe suas vendas e crie metas que te ajudem a alavancar o seu negócio</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Receba o dinheiro das suas vendas na sua conta digital" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Receba o dinheiro das suas vendas na sua conta digital</p>
												</li>
												<div class="mt-16">
													<p class="font-bold paragraph-16">
													</p>
												</div>
												<a href="https://play.google.com/store/apps/details?id=br.com.stone.ton" class="mt-24 btn btn-secondary btn-large">Baixe o app para Android</a>
												<a href="https://apps.apple.com/us/app/ton/id1496404455?l=pt&ls=1" class="mt-24 btn btn-secondary btn-large">Baixe o app para iPhone</a>
											</ul>
										</div>
									</div>
									<div class="Offers_offer__PE9kl">
										<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
											<div style="box-sizing:border-box;display:block;max-width:100%">
												<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDAwIiBoZWlnaHQ9IjQwMCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2ZXJzaW9uPSIxLjEiLz4=" />
											</div>
											<img alt="Venda pelas redes sociais" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_640,q_auto/site-ton/ton-redes-sociais" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
										</div>
										<div>
											<h3>Vendas por redes sociais</h3>
											<h4>Pra você vender sem maquininha</h4>
											<ul>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Gere boletos pra compartilhar e vender por redes sociais" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Gere boletos pra compartilhar e vender por redes sociais</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Envie comprovantes via SMS" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Envie comprovantes via SMS</p>
												</li>
												<li class="flex mt-16 first:mt-0">
													<div class="min-w-[2.4rem] min-h-[2.4rem]">
														<div style="display:inline-block;max-width:100%;overflow:hidden;position:relative;box-sizing:border-box;margin:0">
															<div style="box-sizing:border-box;display:block;max-width:100%">
																<img style="max-width:100%;display:block;margin:0;border:none;padding:0" alt="" aria-hidden="true" role="presentation" src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMjQiIGhlaWdodD0iMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmVyc2lvbj0iMS4xIi8+" />
															</div>
															<img alt="Acompanhe o andamento das suas vendas pelo aplicativo Ton" src="https://res.cloudinary.com/dunz5zfpt/fl_progressive/f_auto,c_limit,w_32,q_auto/v1/site-ton/icons/check" decoding="async" style="position:absolute;top:0;left:0;bottom:0;right:0;box-sizing:border-box;padding:0;border:none;margin:auto;display:block;width:0;height:0;min-width:100%;max-width:100%;min-height:100%;max-height:100%" />
														</div>
													</div>
													<p class="ml-8 paragraph-16">Acompanhe o andamento das suas vendas pelo aplicativo Ton</p>
												</li>
												<div class="mt-16">
													<p class="font-bold paragraph-16">
													</p>
												</div>
												<a href="https://play.google.com/store/apps/details?id=br.com.stone.ton" class="mt-24 btn btn-secondary btn-large">Baixe o app para Android</a>
												<a href="https://apps.apple.com/us/app/ton/id1496404455?l=pt&ls=1" class="mt-24 btn btn-secondary btn-large">Baixe o app para iPhone</a>
											</ul>
										</div>
									</div>
								</div>
							</div>

							<div class="relative">
								<div id="beneficios" class="relative">
								</div>
								<div class="flex flex-col items-center bg-display-50 px-24 md:px-40 py-40 md:py-80 ">
									<h2 class="mb-24 md:mb-40 font-bold text-center heading-2">Por que o Ton é a melhor opção pra você?</h2>
									<div class="flex flex-wrap gap-24 md:gap-40 justify-center w-full max-w-[1280px]">
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M16.336 17.443a1.888 1.888 0 0 0-2.671 0L2.782 29.438a3.763 3.763 0 0 0 0 5.12v.006L13.665 46.56a1.89 1.89 0 0 0 3.224-1.34V18.773c-.001-.499-.2-.977-.553-1.33ZM32 60.414a1.813 1.813 0 0 0 1.193 1.7c.231.084.477.12.723.105C49.714 61.222 62.222 48.077 62.222 32c0-16.076-12.51-29.227-28.306-30.219a1.801 1.801 0 0 0-1.768 1.087 1.81 1.81 0 0 0-.148.718v11.338a2.076 2.076 0 0 0 1.848 2.047 15.103 15.103 0 0 1 9.463 5 15.157 15.157 0 0 1 0 20.05 15.104 15.104 0 0 1-9.463 5A2.07 2.07 0 0 0 32 49.068v11.346Z" fill="#fff" stroke="#00AD0C"></path>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Da Stone para autônomos</h5>
											<p class="paragraph-16">Garantia de qualidade e segurança para seu dinheiro</p>
										</div>
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M62 4a2 2 0 0 0-2-2H30a2 2 0 0 0-2 2v21.839c0 1.677 1.94 2.61 3.25 1.562l6.202-4.963a2 2 0 0 1 1.25-.438H60a2 2 0 0 0 2-2V4Z" fill="#fff" stroke="#00AD0C"></path>
													<path d="M50 44a6 6 0 1 0 0-12 6 6 0 0 0 0 12ZM14 44a6 6 0 1 0 0-12 6 6 0 0 0 0 12ZM24 62a2 2 0 0 0 2-2v-3.965a4 4 0 0 0-1.967-3.449C22.088 51.435 18.716 50 14 50c-4.777 0-8.127 1.426-10.052 2.575A3.996 3.996 0 0 0 2 56.013V60a2 2 0 0 0 2 2h20ZM60 62a2 2 0 0 0 2-2v-3.965a4 4 0 0 0-1.967-3.449C58.088 51.435 54.716 50 50 50c-4.777 0-8.127 1.426-10.052 2.575A3.996 3.996 0 0 0 38 56.013V60a2 2 0 0 0 2 2h20Z" fill="#CDFFC7" stroke="#00AD0C"></path>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Suporte 24h</h5>
											<p class="paragraph-16">Assistente virtual e uma equipe especializada para te atender</p>
										</div>
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M38 12h8a6 6 0 0 1 6 6v2" stroke="#00AD0C"></path>
													<path d="m44 6-6 6 6 6M26 52h-8a6 6 0 0 1-6-6v-2" stroke="#00AD0C"></path>
													<path d="m20 58 6-6-6-6" stroke="#00AD0C"></path>
													<rect x="34" y="26" width="24" height="36" rx="4" fill="#CDFFC7" stroke="#00AD0C">
													</rect>
													<path d="M53 37a1 1 0 0 1-1 1H40a1 1 0 0 1-1-1v-4a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v4Z" fill="#fff" stroke="#00AD0C"></path>
													<circle cx="40" cy="44" r="2" fill="#00AD0C">
													</circle>
													<circle cx="40" cy="50" r="2" fill="#00AD0C">
													</circle>
													<circle cx="40" cy="56" r="2" fill="#00AD0C">
													</circle>
													<circle cx="46" cy="50" r="2" fill="#00AD0C">
													</circle>
													<circle cx="46" cy="44" r="2" fill="#00AD0C">
													</circle>
													<circle cx="46" cy="56" r="2" fill="#00AD0C">
													</circle>
													<circle cx="52" cy="44" r="2" fill="#00AD0C">
													</circle>
													<circle cx="52" cy="50" r="2" fill="#00AD0C">
													</circle>
													<circle cx="52" cy="56" r="2" fill="#00AD0C">
													</circle>
													<rect x="2" y="2" width="24" height="36" rx="4" fill="#CDFFC7" stroke="#00AD0C">
													</rect>
													<path d="M21 13a1 1 0 0 1-1 1H8a1 1 0 0 1-1-1V9a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v4Z" fill="#fff" stroke="#00AD0C"></path>
													<circle cx="8" cy="20" r="2" fill="#00AD0C">
													</circle>
													<circle cx="8" cy="26" r="2" fill="#00AD0C">
													</circle>
													<circle cx="8" cy="32" r="2" fill="#00AD0C">
													</circle>
													<circle cx="14" cy="26" r="2" fill="#00AD0C">
													</circle>
													<circle cx="14" cy="20" r="2" fill="#00AD0C">
													</circle>
													<circle cx="14" cy="32" r="2" fill="#00AD0C">
													</circle>
													<circle cx="20" cy="20" r="2" fill="#00AD0C">
													</circle>
													<circle cx="20" cy="26" r="2" fill="#00AD0C">
													</circle>
													<circle cx="20" cy="32" r="2" fill="#00AD0C">
													</circle>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Troca grátis</h5>
											<p class="paragraph-16">Trocamos a sua maquina sempre que você precisar</p>
										</div>
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M31 28h12" stroke="#00AD0C"></path>
													<path d="M45 22H25a.014.014 0 0 1-.012-.007c-3.27-5.606-8.135-6.349-10.889-6.257-1.15.038-1.792 1.184-1.428 2.276l1.937 5.813c.277.83-.026 1.733-.689 2.304a17.02 17.02 0 0 0-4.673 6.48c-.331.813-1.095 1.39-1.974 1.39H4a2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h5.49c.689 0 1.322.36 1.725.92a17.038 17.038 0 0 0 8.275 6.156c.873.3 1.51 1.091 1.51 2.014V60a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-2a2 2 0 0 1 2-2h6a2 2 0 0 1 2 2v2a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-3.727c0-.832.52-1.566 1.272-1.923C58.02 51.624 62 45.784 62 39c0-9.388-7.611-17-17-17Z" fill="#CDFFC7" stroke="#00AD0C"></path>
													<path d="M19 38a3 3 0 1 0 0-6 3 3 0 0 0 0 6ZM37 14a6 6 0 1 0 0-12 6 6 0 0 0 0 12Z" fill="#fff" stroke="#00AD0C"></path>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Sem aluguel</h5>
											<p class="paragraph-16">Você não paga aluguel e tem acesso ao melhor serviço</p>
										</div>
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path fill="#fff" stroke="#00AD0C" d="M12 10h40v44H12z"></path>
													<path d="M12 54h40v4a4 4 0 0 1-4 4H16a4 4 0 0 1-4-4v-4ZM12 6a4 4 0 0 1 4-4h32a4 4 0 0 1 4 4v4H12V6Z" fill="#CDFFC7" stroke="#00AD0C"></path>
													<path d="M32 17v28M38.75 21.438c-3.242-1.783-13.45-3.112-13.45 3.026 0 7.348 12.967 4.755 12.967 11.238S29.835 41.7 24 39.59" stroke="#00AD0C"></path>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Não precisa de banco</h5>
											<p class="paragraph-16">Conta digital grátis para você receber o seu dinheiro</p>
										</div>
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M57.364 12H6.636C3.523 12 1 14.239 1 17v30c0 2.761 2.523 5 5.636 5h50.728C60.477 52 63 49.761 63 47V17c0-2.761-2.523-5-5.636-5Z" fill="#fff" stroke="#00AD0C"></path>
													<path d="M11.875 19.5V42M52.125 19.5V42M21.93 19.5v14.063M32 19.5V42M42.063 19.5v14.063" stroke="#00AD0C"></path>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Venda por Boleto</h5>
											<p class="paragraph-16">Faça vendas pela internet via boleto no aplicativo da Ton</p>
										</div>
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M2 20h60v32a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V20Z" fill="#fff" stroke="#00AD0C"></path>
													<path d="M2 12a4 4 0 0 1 4-4h52a4 4 0 0 1 4 4v8H2v-8Z" fill="#CDFFC7" stroke="#00AD0C"></path>
													<path d="M30.992 30.71c-1.333-.762-2.992.2-2.992 1.736v9.108c0 1.535 1.659 2.498 2.992 1.736l7.97-4.553c1.343-.768 1.343-2.706 0-3.474l-7.97-4.553Z" stroke="#00AD0C"></path>
													<path d="M47 16a2 2 0 1 0 0-4 2 2 0 0 0 0 4ZM55 16a2 2 0 1 0 0-4 2 2 0 0 0 0 4Z" fill="#00AD0C"></path>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Venda mais</h5>
											<p class="paragraph-16">O Ton te oferece um curso GRÁTIS pra você vender pela internet</p>
										</div>
										<div class="Features_feature__3bzs8 ">
											<div class="mb-16">
												<svg width="64" height="64" fill="none" xmlns="http://www.w3.org/2000/svg">
													<path d="M54 16H10a8 8 0 0 0-8 8v28a8 8 0 0 0 8 8h44a8 8 0 0 0 8-8V24a8 8 0 0 0-8-8Z" fill="#CDFFC7" stroke="#00AD0C"></path>
													<path d="M46.233 25.982C45.973 24.908 45.105 24 44 24H20c-1.105 0-1.974.908-2.233 1.982a7.792 7.792 0 0 1-5.785 5.785C10.908 32.027 10 32.895 10 34v8c0 1.105.908 1.974 1.982 2.233a7.792 7.792 0 0 1 5.785 5.785C18.027 51.092 18.895 52 20 52h24c1.105 0 1.974-.908 2.233-1.982a7.792 7.792 0 0 1 5.785-5.785C53.092 43.973 54 43.105 54 42v-8c0-1.105-.908-1.974-1.982-2.233a7.792 7.792 0 0 1-5.785-5.785Z" fill="#fff" stroke="#00AD0C"></path>
													<path d="M32 44a6 6 0 1 0 0-12 6 6 0 0 0 0 12ZM12 10h40M22 4h20" stroke="#00AD0C"></path>
												</svg>
											</div>
											<h5 class="mb-4 font-bold">Renda extra</h5>
											<p class="paragraph-16">O Ton te ajuda a aumentar sua renda sem precisar sair de casa</p>
										</div>
									</div>
									<a href="{{ route('ton') }}#maquininhas" class="mt-40 btn btn-primary btn-large">Escolha sua maquininha</a>
								</div>
							</div>

							<style type="text/css">
table {
  caption-side: bottom;
  border-collapse: collapse;
}

th {
  text-align: inherit;
  text-align: -webkit-match-parent;
}

thead,
tbody,
tfoot,
tr,
td,
th {
  border-color: inherit;
  border-style: solid;
  border-width: 0;
}

.table {
  --bs-table-bg: transparent;
  --bs-table-accent-bg: transparent;
  --bs-table-striped-color: #212529;
  --bs-table-striped-bg: rgba(0, 0, 0, 0.05);
  --bs-table-active-color: #212529;
  --bs-table-active-bg: rgba(0, 0, 0, 0.1);
  --bs-table-hover-color: #212529;
  --bs-table-hover-bg: rgba(0, 0, 0, 0.075);
  width: 100%;
  margin-bottom: 1rem;
  color: #212529;
  vertical-align: top;
  border-color: #dee2e6;
}

.table > :not(caption) > * > * {
  padding: 0.5rem 0.5rem;
  background-color: var(--bs-table-bg);
  border-bottom-width: 1px;
  box-shadow: inset 0 0 0 9999px var(--bs-table-accent-bg);
}
.table > tbody {
  vertical-align: inherit;
}
.table > thead {
  vertical-align: bottom;
}
.table > :not(:first-child) {
  border-top: 2px solid currentColor;
}

.table-sm > :not(caption) > * > * {
  padding: 0.25rem 0.25rem;
}

.table-bordered > :not(caption) > * {
  border-width: 1px 0;
}
.table-bordered > :not(caption) > * > * {
  border-width: 0 1px;
}

.table-borderless > :not(caption) > * > * {
  border-bottom-width: 0;
}
.table-borderless > :not(:first-child) {
  border-top-width: 0;
}

.table-striped > tbody > tr:nth-of-type(odd) > * {
  --bs-table-accent-bg: var(--bs-table-striped-bg);
  color: var(--bs-table-striped-color);
}

.table-active {
  --bs-table-accent-bg: var(--bs-table-active-bg);
  color: var(--bs-table-active-color);
}

.table-hover > tbody > tr:hover > * {
  --bs-table-accent-bg: var(--bs-table-hover-bg);
  color: var(--bs-table-hover-color);
}

.table-primary {
  --bs-table-bg: #cfe2ff;
  --bs-table-striped-bg: #c5d7f2;
  --bs-table-striped-color: #000;
  --bs-table-active-bg: #bacbe6;
  --bs-table-active-color: #000;
  --bs-table-hover-bg: #bfd1ec;
  --bs-table-hover-color: #000;
  color: #000;
  border-color: #bacbe6;
}

.table-secondary {
  --bs-table-bg: #e2e3e5;
  --bs-table-striped-bg: #d7d8da;
  --bs-table-striped-color: #000;
  --bs-table-active-bg: #cbccce;
  --bs-table-active-color: #000;
  --bs-table-hover-bg: #d1d2d4;
  --bs-table-hover-color: #000;
  color: #000;
  border-color: #cbccce;
}

.table-success {
  --bs-table-bg: #d1e7dd;
  --bs-table-striped-bg: #c7dbd2;
  --bs-table-striped-color: #000;
  --bs-table-active-bg: #bcd0c7;
  --bs-table-active-color: #000;
  --bs-table-hover-bg: #c1d6cc;
  --bs-table-hover-color: #000;
  color: #000;
  border-color: #bcd0c7;
}

.table-info {
  --bs-table-bg: #cff4fc;
  --bs-table-striped-bg: #c5e8ef;
  --bs-table-striped-color: #000;
  --bs-table-active-bg: #badce3;
  --bs-table-active-color: #000;
  --bs-table-hover-bg: #bfe2e9;
  --bs-table-hover-color: #000;
  color: #000;
  border-color: #badce3;
}

.table-warning {
  --bs-table-bg: #fff3cd;
  --bs-table-striped-bg: #f2e7c3;
  --bs-table-striped-color: #000;
  --bs-table-active-bg: #e6dbb9;
  --bs-table-active-color: #000;
  --bs-table-hover-bg: #ece1be;
  --bs-table-hover-color: #000;
  color: #000;
  border-color: #e6dbb9;
}

.table-danger {
  --bs-table-bg: #f8d7da;
  --bs-table-striped-bg: #eccccf;
  --bs-table-striped-color: #000;
  --bs-table-active-bg: #dfc2c4;
  --bs-table-active-color: #000;
  --bs-table-hover-bg: #e5c7ca;
  --bs-table-hover-color: #000;
  color: #000;
  border-color: #dfc2c4;
}

.table-light {
  --bs-table-bg: #f8f9fa;
  --bs-table-striped-bg: #ecedee;
  --bs-table-striped-color: #000;
  --bs-table-active-bg: #dfe0e1;
  --bs-table-active-color: #000;
  --bs-table-hover-bg: #e5e6e7;
  --bs-table-hover-color: #000;
  color: #000;
  border-color: #dfe0e1;
}

.table-dark {
  --bs-table-bg: #212529;
  --bs-table-striped-bg: #2c3034;
  --bs-table-striped-color: #fff;
  --bs-table-active-bg: #373b3e;
  --bs-table-active-color: #fff;
  --bs-table-hover-bg: #323539;
  --bs-table-hover-color: #fff;
  color: #fff;
  border-color: #373b3e;
}

.table-responsive {
  overflow-x: auto;
  -webkit-overflow-scrolling: touch;
}

tr, th {
	border-bottom: 0px;
}

td {
	padding: 7px 15px !important;
}

.align-middle {
  vertical-align: middle !important;
}


							</style>

							<div class="hidden xs:block w-full">
								<div id="compare-as-taxas" class="anchorV3"></div>
								<div class="DownloadApp_wrapper__7h2G-">
									<h2 class="font-bold heading-2">Por que o Ton é a melhor opção entre todas?</h2>
									<h3 class="font-normal heading-3 mb-24">Compare as taxas das principais maquininhas do mercado:</h3>
									<div class="table-responsive">
										<table class="mb-40 font-normal table table-striped table-hover paragraph-14">
											<thead>
												<tr class="paragraph-16 align-middle">
													<th rowspan="2">Maquininha</th>
													<th rowspan="2">Débito</th>
													<th colspan="12">Crédito</th>
												</tr>
												<tr class="align-middle">
													<th>1x</th>
													<th>2x</th>
													<th>3x</th>
													<th>4x</th>
													<th>5x</th>
													<th>6x</th>
													<th>7x</th>
													<th>8x</th>
													<th>9x</th>
													<th>10x</th>
													<th>11x</th>
													<th>12x</th>
												</tr>
											</thead>
											<tbody>
@foreach($data["pos"] as $item)
@php
$last = $loop->last;
$even = $loop->even;
@endphp
												<tr class="align-middle{{ ( $last ? ' paragraph-18 font-bold' : '' ) }}">
@foreach($item as $i)
@if(is_string($i))
													<td>{{ $i }}</td>
@else
													<td>{{ number_format($i, 2, ",", ".") }}%</td>
@endif
@endforeach
												</tr>
@endforeach
											</tbody>
										</table>
									</div>
									<p class="paragraph-14 mb-24">
										*Todas as taxas são para recebimento no próximo dia útil.
										<br />
										Taxas atualizadas em {{ Carbon\Carbon::createFromTimestamp($data["date"])->format("d/m/Y \à\s H:i") }}.
									</p>
									<a href="{{ route('ton') }}#maquininhas" class="mb-40 btn btn-secondary btn-large">Não perca tempo. Escolha sua maquininha!!</a>
								</div>
							</div>

							<script nomodule="" src="https://www.ton.com.br/_next/static/chunks/polyfills-4b807e79471bfa329e5d.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/9492.2992b511035a0967fd93.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/2550.c00df0c44b8785602d5a.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/7174.b64609cb624a67ee80da.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/8687.9e34e8ff6a971e59d6b9.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/8105.ac010b3120a3e6281cbb.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/957.ee54d989f86e59caf67e.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/9557.550f1459268d5e754608.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/7361.95b73a7123ad62b96b81.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/4281.d2056bb04c5e0d8c868a.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/4210.9800ea038e334500d790.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/1850.d66c3c7a68683433c5d5.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/8731.ed6cf65c38e67fedef91.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/221.d4660a6c3eebfb53e337.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/1824.aeac7f23bf47c82d58e0.js"></script>
							<script async="" src="https://www.ton.com.br/_next/static/chunks/6176-c42d952c50f2f6b42c33.js"></script>
							<script src="https://www.ton.com.br/_next/static/chunks/webpack-f494fdd072920e76f5c3.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/framework-9260c7ce6eb8840e551e.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/main-d550d58322f8445a98e1.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/pages/_app-ef0d0b040a803001b83a.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/2455-28fdbde43e9e582db6d8.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/7901-baa24bff990ebbdf91ce.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/1268-6edd7988ebdc139c47ba.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/9591-2b894b26ef83de15f567.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/4687-4401d415952335433cad.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/8988-16d195709aa539216d84.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/chunks/pages/index-67d50b3c3c4795bfa92d.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/g5rsU8cpD9S_FUnvB_V9o/_buildManifest.js" async=""></script>
							<script src="https://www.ton.com.br/_next/static/g5rsU8cpD9S_FUnvB_V9o/_ssgManifest.js" async=""></script>
							<next-route-announcer>
								<p aria-live="assertive" id="__next-route-announcer__" role="alert" style="border: 0px none; clip: rect(0px, 0px, 0px, 0px); height: 1px; margin: -1px; overflow: hidden; padding: 0px; position: absolute; width: 1px; white-space: nowrap; overflow-wrap: normal;">
								</p>
							</next-route-announcer>
							<script src="https://www.ton.com.br/_next/static/chunks/pages/maquininha/%5BmachineName%5D-f508bab831b3c2d24f7d.js"></script>
							<style>@keyframes default-botfront-blinker-animation{from{outline-color:green;outline-style:none}to{outline-style:solid;outline-color:green}}</style>
							<script src="https://www.ton.com.br/_next/static/chunks/8172-c78084adaaab5e794e5a.js"></script>
							<script src="https://www.ton.com.br/_next/static/chunks/pages/tapton-b1719b5a246b773b55d7.js"></script>
							<script src="https://www.ton.com.br/_next/static/chunks/pages/planos-e-taxas-feb1972fc572cac0cd45.js"></script>
						</body>
						</html>
